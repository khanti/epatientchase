{
    "snippet": {
        "key": "dbc607f8e1fb48ecbff8da05b3cbf4df",
        "title": "Creating mail merged letters",
		"summary": "Export patients into personalised mail merged Word letters",
        "content": [
            "section",
            {
                "link": "Click here to download pdf file of this section",
                "url": "../../Resources/Letters.pdf"
            },
            {
                "list": "In this section you will be shown how to:",
                "items": [
                    "Export your list of patients into a Word mail merge which results in personalised letters that are ready to send",
                    "Send letters in batches",
                    "Send letters to a bespoke selection of patients",
                    "Record letters as viewable attachments into each patient’s EMIS medical record",
                    "Batch record Read codes into EMIS"
                ]
            },
            {
                "image": "../../Resources/Letters/letters_carousel.png"
            },
            {
                "paragraph": "Once you’ve compiled your list of patients to contact, select the letter template you’ll be using and PatientChase does the rest. It’ll produce informative patient letters that are personalised to each patient – only information specific to them will appear on each letter."
            },
            {
                "paragraph": "You can decide how many letters to send out each time and PatientChase will remember where you left off, bookmarked ready for the next time you decide to send out letters.  It will also know who has been sent 1, 2 or 3 letters so you don’t have to keep check. It will record every incident of you contacting them."
            },
            {
                "paragraph": "<strong>Step 1 – Select Letter as the media you will be using</strong>"
            },
            {
                "image": "../../Resources/Letters/select_letter.png"
            },
            {
                "paragraph": "Right click over your search results and select ‘Letter’ under ‘Media to Use for Search’."
            },
            {
                "paragraph": "This means that you will be creating mail merges."
            },
            {
                "paragraph": "<strong>Step 2 – Create your mail merge and record into EMIS</strong>"
            },
            {
                "image": "../../Resources/Letters/select_export_to_letter_and_insert_into_EMIS.png"
            },
            {
                "paragraph": "Right click on ‘Results’ under your search name and select ‘Export to Letter AND Insert into EMIS’."
            },
            {
                "paragraph": "Selecting this option will create mail merged patient letters AND place a viewable copy of each letter in each patient’s medical record AND batch Read code into their medical records that they have been contacted."
            },
            {
                "paragraph": "NOTE: If you select the ‘Export Results to Letter Only’ or ‘Insert Letter into EMIS Only’ options then you’ll be able to mail merge and record into EMIS in separate stages."
            },
            {
                "paragraph": "<strong>Step 3 – Confirm that you wish to create a mail merge and copy into EMIS</strong>"
            },
            {
                "image": "../../Resources/Letters/letter_input_required.png"
            },
            {
                "paragraph": "Click OK."
            },
            {
                "paragraph": "<strong>Step 4 – Decide how many patients you want to send letters to in your list</strong>"
            },
            {
                "paragraph": "You can send letters to everyone in your list, or if the list is very long, you can send letters in batches. If you specify a number of patients then PatientChase will create a mail merge just for those patients, starting from the first patient in your list."
            },
            {
                "image": "../../Resources/Letters/letter_input_required_2.png"
            },
            {
                "paragraph": "In the example above, we’re going to create a mail merge for the first 10 patients."
            },
            {
                "paragraph": "Define how many patients to contact and click OK."
            },
            {
                "paragraph": "PatientChase will record and bookmark that you have sent letters to the first 10 patients only and will remember where you left off for the next time you send out letters."
            },
            {
                "paragraph": "<strong>Step 5 – Select the letter template to use</strong>"
            },
            {
                "image": "../../Resources/Letters/letter_select_template.png"
            },
            {
                "paragraph": "The template determines the content style of your mail merge."
            },
            {
                "paragraph": "Pick your template and Click OK."
            },
            {
                "paragraph": "<strong>Step 6 – Choose the date of insert into EMIS</strong>"
            },
            {
                "paragraph": "This will insert a date in the patient’s record for the viewable letter attachment and the Read codes."
            },
            {
                "paragraph": "It will also insert this date into the first column of PatientChase’s list of patients."
            },
            {
                "image": "../../Resources/Letters/letter_select_date_of_insert.png"
            },
            {
                "paragraph": "Select the date and click OK."
            },
            {
                "paragraph": "A progress bar will appear and then your mail merged letters will appear in Word."
            },
            {
                "paragraph": "If you go back to PatientChase you will see the ‘DateInserted’ column will have date entries against the patients you’ve mail merged."
            },
            {
                "image": "../../Resources/Letters/letter_date_inserted.png"
            },
            {
                "paragraph": "Notice also the bookmark is on the 10th patient, indicating that your next batch to send will start from the 11th patient. Follow steps 2 to 6 to send your next batch."
            },
            {
                "paragraph": "<strong>Step 7 – Selecting a bespoke list of patients to create mail merged letters for (Optional)</strong>"
            },
            {
                "paragraph": "As an alternative to sending letters in batches, you can manually select specific patients in the list and send letters to just them."
            },
            {
                "image": "../../Resources/Letters/letter_bespoke_selection.png"
            },
            {
                "paragraph": "Click on the first patient you’d like to mail merge then hold the CTRL key down and click on the other patients you’d like to include in your mail merge."
            },
            {
                "paragraph": "Right click over the list of patients and select ‘Export Selected Patients to Letter AND Insert into EMIS’."
            },
            {
                "paragraph": "Then follow steps 3, 5, 6. "
            },
            {
                "image": "../../Resources/Letters/letter_bespoke_selection_export.png"
            },
            {
                "paragraph": "Or, if it’s a continual selection of patients you’d like to mail merge, click on the first patient, hold the SHIFT key down and click on the last patient to include in your mail merge."
            },
            {
                "paragraph": "Then follow steps 3, 5, 6. "
            }
        ],
        "video": "http://www.youtube.com/watch?v=Qm09tZhzfjs"
    }
}