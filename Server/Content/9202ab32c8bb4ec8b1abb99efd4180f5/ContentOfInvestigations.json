{
    "snippet": {
        "key": "9202ab32c8bb4ec8b1abb99efd4180f5",
        "title": "Investigation Template",
		"summary": "How to put outstanding blood, urine and Investigations in to a letter",
        "content": [
            "section",
            {
                "link": "click here to download the pdf version of this file",
                "url": "../../Resources/Investigations.pdf"
            },
            {
                "list": "In this section you will be shown how to:",
                "items": [
                    "Add the relevant merge fields in a letter template to display the investigations that the patient requires",
                    "Define which conditions are linked to each investigation"
                ]
            },
            {
                "image": "../../Resources/investigations/vialofblood.png"
            },
            {
                "paragraph": "<strong>Example of use:</strong>"
            },
            {
                "paragraph": "Depending upon which conditions the patient has, the list of associated investigations required will be displayed on their invite letter, <strong>e.g:  full blood count, Gamma GT, Urine for Protein, BMI.</strong> This can be particularly useful for the nurse."
            },
            {
                "image": "../../Resources/investigations/Investigationsexample.png"
            },
            {
                "paragraph": "<strong>Step 1 – Selecting the investigation merge fields to use</strong>"
            },
            {
                "paragraph": "Open your letter template in Word (by going to Admin>Edit>Letters)."
            },
            {
                "paragraph": "Copy and paste in any of the following merge fields (and wording) into your template:"
            },
            {
                "paragraph": "<div>You need the following Blood Test/s: [Blood]</div><div>You need the following Urine Sample: [Urine]</div><div>You need the following Other Test/s: [OtherTests]</div><div>Is a fasting blood test required?</div><div>[FastingYesNo]</div><div>[FastingText]</div>"
            },
            {
                "paragraph": "In Word, click Save."
            },
            {
                "paragraph": "<strong>Step 2 – Linking conditions to investigations</strong>"
            },
            {
                "paragraph": "You are provided with a pre-populated list of which investigations the patient might need. The investigations appearing on the patient’s letter will depend upon which conditions the patient is being recalled for."
            },
            {
                "paragraph": "You can change which conditions are associated with each investigation."
            },
            {
                "paragraph": "Go to Admin>Edit>Investigations."
            },
            {
                "paragraph": "The ‘Associated Conditions’ column shows which conditions are linked to each investigation."
            },
            {
                "image": "../../Resources/investigations/Investigationslinking.png"
            },
            {
                "paragraph": "To change this, click on any of the cells in this column."
            },
            {
                "paragraph": "In this example we’re going to look at how to change the conditions linked to a patient requiring a cholesterol test."
            },
            {
                "image": "../../Resources/investigations/Investigationslinking_2.png"
            },
            {
                "paragraph": "Select/deselect the conditions you wish to link."
            },
            {
                "paragraph": "Click Close."
            },
            {
                "paragraph": "<strong>Step 3 – Determine whether investigations should only be linked with outstanding QOF targets</strong>"
            },
            {
                "paragraph": "The Always Show column indicates whether the investigation should always appear on the patients letter, irrespective of whether they have achieved the relevant QOF targets in that condition."
            },
            {
                "paragraph": "Tick each investigation where this applies."
            },
            {
                "paragraph": "Untick those investigations where you only want them appearing on a letter if the patient has relevant outstanding QOF targets."
            },
            {
                "image": "../../Resources/investigations/Investigationsalwaysshow.png"
            },
            {
                "paragraph": "<strong>Step 4 – Determine which investigations require fasting</strong>"
            },
            {
                "paragraph": "Provided you have the following merge fields in your letter template:"
            },
            {
                "paragraph": "<div>Is a fasting blood test required?</div><div>[FastingYesNo]</div><div>[FastingText]</div>"
            },
            {
                "paragraph": "then you link fasting text to each investigation."
            },
            {
                "image": "../../Resources/investigations/Investigationsfastinglinks.png"
            },
            {
                "paragraph": "Select/deselect the investigations you wish to link."
            },
            {
                "paragraph": "<strong>Step 5 – Check/modify the fasting/non fasting wording.</strong>"
            },
            {
                "paragraph": "Click in either/both boxes to reword."
            },
            {
                "image": "../../Resources/investigations/Investigationsfastingprose.png"
            },
            {
                "paragraph": "When you’ve finished modifying Investigations, click ‘Back to Patientchase’."
            }
        ],
        "video": "http://www.youtube.com/watch?v=Qm09tZhzfjs"
    }
}