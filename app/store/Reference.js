Ext.define('ePatientChase.store.Reference', {
    extend: 'Ext.data.Store',

    storeId:'reference',
    fields:['Reference'],
    autoLoad: 'true',
    proxy: {
        type: 'ajax',
        url : ePatientChase.Utilities.baseURL + 'Reference.json',
        reader: {type: 'json', root: 'snippets'}
    }
});