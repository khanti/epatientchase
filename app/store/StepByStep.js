Ext.define('ePatientChase.store.StepByStep', {
    extend: 'Ext.data.Store',

    storeId:'stepbystep',
    fields:['StepByStep'],
    autoLoad: 'true',
    proxy: {
        type: 'ajax',
        url : ePatientChase.Utilities.baseURL + 'StepByStep.json',
        reader: {type: 'json', root: 'StepByStep'}
    }
});