Ext.define('ePatientChase.store.QA', {
    extend: 'Ext.data.Store',

    storeId:'qa',
    fields:['QA'],
    autoLoad: 'true',
    proxy: {
        type: 'ajax',
        url : ePatientChase.Utilities.baseURL + 'QuestionsAndAnswers.json',
        reader: {type: 'json', root: 'QuestionsAndAnswers'}
    }
});