Ext.define('ePatientChase.store.Snippets', {
    extend: 'Ext.data.Store',

    storeId:'snippets',
    fields:['snippet'],
    autoLoad: 'false',
    proxy: {
        type: 'localstorage',
        id:   'snippetproxy'
    }
});