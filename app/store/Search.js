Ext.define('ePatientChase.store.Search', {
    extend: 'Ext.data.Store',

    storeId:'search',
    fields:['Search'],
    autoLoad: 'true',
    proxy: {
        type: 'ajax',
        url : ePatientChase.Utilities.baseURL +'Search.json',
        reader: {type: 'json', root: 'Search'}
    }
});