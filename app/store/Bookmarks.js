Ext.define('ePatientChase.store.Bookmarks', {
    extend: 'Ext.data.Store',

    storeId:'bookmark',
    fields:['name', 'snippets_id', 'snippets_filename', 'url'],
    autoLoad: 'true',
    proxy: {
        type: 'localstorage',
        id:   'bookmarkproxy'
    }
});