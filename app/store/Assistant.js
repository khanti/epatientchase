Ext.define('ePatientChase.store.Assistant', {
    extend: 'Ext.data.Store',

    storeId:'assistant',
    fields:['Assistant'],
    autoLoad: 'true',
    proxy: {
        type: 'ajax',
        url : ePatientChase.Utilities.baseURL +'Assistant.json',
        reader: {type: 'json', root: 'Assistant'}
    }
});