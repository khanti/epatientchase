Ext.define('ePatientChase.store.Home', {
    extend: 'Ext.data.Store',

    storeId:'home',
    fields:['Home'],
    autoLoad: 'true',
    proxy: {
        type: 'ajax',
        url : ePatientChase.Utilities.baseURL +'Home.json',
        reader: {type: 'json', root: 'Home'}
    }
});