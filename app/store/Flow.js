Ext.define('ePatientChase.store.Flow', {
    extend: 'Ext.data.Store',

    storeId:'flow',
    fields:['snippets'],
    autoLoad: 'true',
    proxy: {
        type: 'ajax',
        url : ePatientChase.Utilities.baseURL +'Flow.json',
        reader: {type: 'json', root: 'snippets'}
    }
});