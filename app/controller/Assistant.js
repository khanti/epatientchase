Ext.define('ePatientChase.controller.Assistant', {
    extend: 'ePatientChase.controller.TabController',
    stores:['Assistant','Flow'],
    requires: ['Ext.window.MessageBox'],
    refs:[
        {
            ref:'assistantview',
            selector:'assistantview'

        },{
            ref:'carouselcontainer',
            selector:'carouselcontainer'
        },
        {
            ref:'breadCrumbs',
            selector:'#breadcrumbs'
        }
    ],
    baseSentence:"",
    HotText:"",
    SentenceOptions:[],
    activeHotText:0,
    currentStep:0,
    myDescription:"description",
    init: function() {

        this.getFlowStore().on('load',this.onFlowLoad,this);


        this.control({
            '#testbutton': {
                click: 'setContent'
            },
            '#assistantStoryBoardButton':{
              click:'createStoryBoard'
            },
            '#assistantResetButton':{
              click:'resetSentence'
            },
            'hotlistitem':
            {
                mouseover:'populateCarousel',
                click:'doNothing'
            },
            'assistantview':{
                'carouselItemChanged':'carouselItemChanged',
                'carouselItemClicked':'carouselItemClicked',
                show:'onShow'
            },
            '#prevStepBtn':{
               click:'prevStep'
            },
            '#nextStepBtn':{
                click:'nextStep'
            },

            '#thecarousel':{
                click:'showCurrentSnippet'
            },
            '#bookmarkAssistantButton':{
                click:'addBookmark'
            }

        });
    },
    /*
    addBookmark:function(){//old one
    var mySnippets = [];


        Ext.MessageBox.buttonText.ok = "Yes";
        Ext.MessageBox.buttonText.cancel = "No";
        for(var i = 0; i < this.HotText.length; i++){
            if( this.HotText[i][this.SentenceOptions[i]].id != 0)
                mySnippets.push({id:this.HotText[i][this.SentenceOptions[i]].id , FileName:this.HotText[i][this.SentenceOptions[i]].FileName});
        }
  //      Ext.Msg.alert('Status', 'Changes saved successfully.');

        Ext.Msg.prompt('Create Bookmark?', 'Bookmark name:', function(btn, text){
            if (btn == 'ok'){
                // process text value and close...
               console.log(mySnippets);
                console.log("%%%%");
                ePatientChase.Utilities.bookmarks.push({name:text, snippets:mySnippets, url:'a url'});
                console.log(ePatientChase.Utilities.bookmarks);
            //    Ext.util.Cookies.
            }
        });
    },*/

    addBookmark:function(){
        var mySnippetsID = [];
        var mySnippetsFileName = [];

        Ext.MessageBox.buttonText.ok = "Yes";
        Ext.MessageBox.buttonText.cancel = "No";

        for(var i = 0; i < this.HotText.length; i++){
            if( this.HotText[i][this.SentenceOptions[i]].id != 0){


                mySnippetsID.push(this.HotText[i][this.SentenceOptions[i]].id);
               mySnippetsFileName.push(this.HotText[i][this.SentenceOptions[i]].FileName);
                }
        }


        Ext.Msg.prompt('Bookmark Storyboard?', 'Bookmark name:', function(btn, text){
            if (btn == 'ok'){

                var Bookmark = StackMob.Model.extend({ schemaName: 'bookmark' });
                var myBookmark = new Bookmark({bookmark_name:text, snippets_id:mySnippetsID, snippets_filename:mySnippetsFileName});

                myBookmark.create({
                    success: function(model) {

                        var theID = model.get('bookmark_id');
                        Ext.getStore('Bookmarks').add({name:text, snippets_id:mySnippetsID, snippets_filename:mySnippetsFileName, url:theID});
                        Ext.getStore('Bookmarks').sync();
                    },
                    error: function(model, response) {
                        Ext.Msg.alert('Sorry', 'Cannot connect to the server. Bookmark will be stored locally.');
                        Ext.getStore('Bookmarks').add({name:text, snippets_id:mySnippetsID, snippets_filename:mySnippetsFileName, url:'off line'});
                        Ext.getStore('Bookmarks').sync();
                    }
                });
            }
        });
    },

    onShow:function(){
        var data =  this.HotText[0];
        this.activeHotText = 0;
        this.getCarouselcontainer().setCarouselData(data, 0);
        var data =  this.HotText[0];
        this.activeHotText = 0;
        this.getCarouselcontainer().setCarouselData(data, 0);
   //     Ext.get('breadcrumbs').setStyle('font-size', '76px');
        if( Ext.getBody().getViewSize().height <= 760){
            this.getAssistantview().child('topsectionview').hide(true);
            this.getAssistantview().child('bottomsectionview').hide(true);

            Ext.get('breadcrumbs').setStyle('width', '760px');
     //   alert(   Ext.get('breadcrumbLabel'));
            return;
        }

        var me = this;
        setTimeout(function(){
            var topMargin =  Ext.getBody().getViewSize().height/2 - 398;
          //var leftMargin = Ext.getBody().getViewSize().width/2 - 512;
            leftMargin =0;
            me.getAssistantview().getComponent(1).setMargin(topMargin + " 0 0 " + leftMargin);
            me.getAssistantview().getComponent(1).setHeight(2000);
        },100);


        //this.getBreadCrumbs().setX( this.getCarouselcontainer().getX());

    },
    onFlowLoad:function(){


        if( this.getAssistantStore() == null ||  this.getAssistantStore() == undefined)
           this.getAssistantStore().on('load',this.setContent,this);
        else
            this.setContent();
    },
    createStoryBoard:function(){
        var me = this;
     //   this.getAssistantview().child('middlesectionview').getComponent(2).getComponent(2).setVisible(true);
        me.clearSnippets();
        me.dataidCache = [];
        me.dataFileNameCache = [];

        for(var i = 0; i < this.HotText.length; i++){
            if( this.HotText[i][this.SentenceOptions[i]].id != 0){
                me.dataidCache.push(this.HotText[i][this.SentenceOptions[i]].id);
                me.dataFileNameCache.push(this.HotText[i][this.SentenceOptions[i]].FileName);
            }
               // me.addSnippet(this.HotText[i][this.SentenceOptions[i]].id , this.HotText[i][this.SentenceOptions[i]].FileName);
        }
        this.showStory(me.dataidCache, me.dataFileNameCache );
    },

    dataidCache:[],
    dataFileNameCache:[],
    showStory:function(dataid, dataFileName){

        var me = this;
        me.dataidCache = dataid;
        me.dataFileNameCache = dataFileName;
        this.clearSnippets();


        for(var s in dataid){
            setTimeout(function(){
                me.popSnippetToShow();

            }, (s+1)*30);

        }

    },
    popSnippetToShow:function(){
        var me = this;


        this.addSnippet(  me.dataidCache[0], me.dataFileNameCache[0]);
        me.dataidCache.splice(0,1);  me.dataFileNameCache.splice(0,1);
    },
    prevStep:function(){
        if(this.currentStep > 0)
            this.currentStep--;
        if(this.currentStep == 0)
            this.getAssistantview().child('middlesectionview').getComponent(1).getComponent(0).animate({to:{'opacity':'0'}});
        this.getAssistantview().child('middlesectionview').getComponent(1).getComponent(2).animate({to:{'opacity':'1'}});

        this.getAssistantview().child('middlesectionview').getComponent(1).getComponent(2).getComponent(1).setText("Next");
        this.getAssistantview().child('middlesectionview').getComponent(1).getComponent(2).getComponent(0).setGlyph('59226@Entypo');
        this.getAssistantview().child('middlesectionview').getComponent(1).getComponent(2).getComponent(2).setVisible(false);
        this.populateCarousel( this.currentStep );
        this.updateSentence();

        //updated everything
    },
    nextStep:function(){

        if( this.currentStep == (this.HotText.length -1 ) ) {
            this.createStoryBoard();
            this.addBookmark();
        }else{
            if(this.currentStep < (this.HotText.length - 1))
                this.currentStep++;

            if( this.currentStep == (this.HotText.length -1 ) ){
                this.getAssistantview().child('middlesectionview').getComponent(1).getComponent(2).getComponent(0).setGlyph('128077@Entypo');
                this.getAssistantview().child('middlesectionview').getComponent(1).getComponent(2).getComponent(1).setText("Finish");
            }


            this.getAssistantview().child('middlesectionview').getComponent(1).getComponent(0).animate({to:{'opacity':'1'}});
            this.populateCarousel( this.currentStep );
            this.updateSentence();

        }

    },
    resetSentence:function(){
        var me = this;
        me.clearSnippets();

        for(var i = 0; i < this.HotText.length; i++){
            this.SentenceOptions[i] = this.HotText[i].length - 1;

        }

     //   this.updateSentence();

        var data =  this.HotText[this.activeHotText];
        var firstSlide =  this.HotText[this.activeHotText].length - 1;
        this.currentStep = 0;
        this.getCarouselcontainer().setCarouselData(data, firstSlide);
        this.getAssistantview().child('middlesectionview').getComponent(1).getComponent(0).animate({to:{'opacity':'0'}});
        this.getAssistantview().child('middlesectionview').getComponent(1).getComponent(2).animate({to:{'opacity':'1'}});
     //   this.updateSentence();
        this.populateCarousel( this.currentStep );
    },

    setContent:function(){

        var me = this;




        this.setMyView(this.getAssistantview());

        var topSection    = this.getAssistantview().child('topsectionview');
        var middleSection = this.getAssistantview().child('middlesectionview');
        var bottomSection = this.getAssistantview().child('bottomsectionview');

        var data = this.getAssistantStore().getAt(0).raw;


        this.parseFlow();
        topSection.update({'Title':data.Title,'Description':this.myDescription});

        this.populateHotList();



    },

    populateHotList:function(){
        return;
        var container = this.getAssistantview().child('middlesectionview').getComponent(0);

        for(var i = 0; i < this.HotText.length; i++){

            container.add({'xtype':'hotlistitem', 'text':this.HotText[i][0].parent});
        }

    },
    populateCarousel:function( index){
        //this.getCarouselcontainer();
        //this.getAssistantview().child('middlesectionview').getComponent(1).getComponent(1).child('label').setText( "Choose your " + this.HotText[index][0].parent );
        this.getAssistantview().child('middlesectionview').getComponent(1).getComponent(1).child('label').setText( this.HotText[index][0].parent );
        //    console.log( this.getAssistantview().child('middlesectionview').child('label'));
        var data =  this.HotText[index];
        var firstSlide = this.SentenceOptions[index];
        this.activeHotText = index;
        this.getCarouselcontainer().setCarouselData(data, firstSlide);


    },
    carouselItemChanged:function(opt){
var me = this;
        this.SentenceOptions[this.activeHotText] =  opt.itemIndx;
       setTimeout(function(){ me.updateSentence();},200);

    },
    carouselItemClicked:function(opt){

        this.showCurrentSnippet();
    },
    parseFlow:function(){
        this.HotText = [];

        var flowData = this.getFlowStore().getAt(0).raw;
        var s        = this.getAssistantStore().getAt(0).raw.header[0].paragraph ;//+ this.getAssistantStore().getAt(0).raw.header[1].paragraph; //s is for sentence
        var bFinished = false;
        var newSentence = "";
        var myTag ="";
        var indx=0;
        var lastIndx = 0;
        var hotTextIndx = 0;

        console.log(flowData);



        while( bFinished == false){

            var indx= s.indexOf("[",lastIndx);
            var bParsed = false;

            var snippetArray = [];

            if(indx == -1){
                bFinished = true;
                break;
            }

            hotTextIndx++;
            var snipIndx = indx + 1;

            while( bParsed == false){
                var guid = s.substring(snipIndx, snipIndx + 32);
              //  console.log(guid);
                var theSnippet = this.getSnippet(guid);
              //  console.log(theSnippet);

                if(theSnippet != undefined){
                    snippetArray.push(theSnippet);
                }
                else
                    console.log("WARNING: snippet not found " + guid);

                snipIndx += 32;

                 if( s[snipIndx] == ']')
                    bParsed = true;
                 else
                     snipIndx++;
            }
            snippetArray.push({"name":"Skip","id":"0","summary":""});
            newSentence += s.substring(lastIndx, indx) + "[HotText"+ hotTextIndx + "";
            this.HotText.push(snippetArray);
            this.SentenceOptions.push(0);
            lastIndx = s.indexOf("]",indx);

        }

        newSentence +=  s.substring(lastIndx, s.length);

        this.baseSentence = newSentence;

        console.log(this.HotText);
        console.log(this.baseSentence);

    },
    getSnippet:function(guid){
        var flowData = this.getFlowStore().getAt(0).raw;
        var myGUID = guid;
        var mySnippet;

        this.traverse(flowData,function(key, value, par, parpar, parparpar){
       //     if(key.snippets != undefined)
          //  console.log(value + " " + myGUID);
            if(key == "id"  && value == myGUID){
            // console.log(value);
                mySnippet =  par;
                mySnippet.parent = parparpar.name;

            }
            //if(key.id != undefined)
           //   console.log(key + " : "+value);
        },undefined);

        return mySnippet;
    },
    getUpdatedSentece:function(){
        var s ="";
        var i;

        for( i = 0; i < (this.currentStep + 1); i++){

            s += "<b>" +  this.HotText[i][0].parent + "</b> > " +  this.HotText[i][this.SentenceOptions[i]].name;
            if(i < (this.HotText.length - 1)) s+= " > ";
/*
            var myText = this.HotText[i-1][this.SentenceOptions[i-1]].name;

            if((i - 1) == this.activeHotText)
            s = s.replace("[HotText" + i + "]" ,"<span style='color:rgb(18, 160, 255); font-size: 1.2em;'><b>[" + myText + "]</b></span>"  );
            else
                s = s.replace("[HotText" + i + "]" ,"<b>[" + myText + "]</b>"  );
*/
        }

        return s;
    },
    updateSentence:function(){
        var topSection    = this.getAssistantview().child('topsectionview');
        var data = this.getAssistantStore().getAt(0).raw;
        var me = this;

        topSection.update({'Title':data.Title, 'Description':data.description});

        var breadCrumbs = this.getBreadCrumbs();

        var baseX = this.getAssistantview().child('middlesectionview').getComponent(1).getComponent(1).getX();

        this.getBreadCrumbs().update({html:this.getUpdatedSentece()});

            if(breadCrumbs.getWidth() >= 800){
                breadCrumbs.setX(baseX - ((breadCrumbs.getWidth()-800)/2));
            }
            else
            {
                breadCrumbs.setX(this.getCarouselcontainer().getX());
            }

    },
    traverse:function(o,func, parpar, parparpar) {
    for (var i in o) {
        func.apply(this,[i,o[i], o, parpar, parparpar]);
        if (typeof(o[i])=="object") {
            //going on step down in the object tree!!
            this.traverse(o[i],func, o, parpar);
        }
    }

    },
    showCurrentSnippet:function(){
        if(this.HotText[this.currentStep][this.SentenceOptions[this.currentStep]].name == "None") return;

        this.showSnippet(this.HotText[this.currentStep][this.SentenceOptions[this.currentStep]].id,
            this.HotText[this.currentStep][this.SentenceOptions[this.currentStep]].FileName);

      //  alert( this.HotText[this.currentStep][this.SentenceOptions[this.currentStep]]);
    },
    doNothing:function(){
    }

});