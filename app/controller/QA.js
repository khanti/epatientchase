Ext.define('ePatientChase.controller.QA', {
    extend: 'ePatientChase.controller.TabController',
    stores:['QA'],
    requires:['Ext.form.Label'],
    refs:[
        {
            ref:'qaview',
            selector:'qaview'
        },
        {
            ref:'treePanel',
            selector:'#qaTreePanel'
        }
    ],
    root:null,
    init: function() {

        this.control({
            '#qaResetButton':{
               click:'resetQA'
            },
            'qaview':{
                show:'onShow',
                resetQA:'resetQA'
            }
        });

        this.setMyView(this.getQaview());
        this.getQAStore().on('load',this.setContent,this);

 
    },

    setContent:function(){
        var i, j;
        var topSection    = this.getQaview().child('topsectionview');
        var middleSection = this.getQaview().child('middlesectionview');
        var bottomSection = this.getQaview().child('bottomsectionview');

        var data = this.getQAStore().getAt(0).raw;

        topSection.update({'Title':data.Title,'Description':data.description});

        console.log(data.body);
        console.log(this.getTreePanel());

        if(this.root == null){
            this.root = new Object();

            this.root.text = "Questions and Answers";
            this.root.expanded = false;
            this.root.children = new Array();
        }
      //  node.children[indx].children.push({text:data[d].snippets[d2].name, leaf:true, checked:false,

            for(i in data.body){
                this.root.children.push({text: '<span style="font-size:1.25em">'+ data.body[i].name +'</span>', leaf:false});
                var indx = this.root.children.length - 1;
                this.root.children[indx].children = new Array();

                for(j in data.body[i].snippets){
                    this.root.children[indx].children.push({text:'<span style="font-size:1.1em">'+ data.body[i].snippets[j].question+'</span>', leaf:false});
                    this.root.children[indx].children[j].children = new Array({ text:'<span style="color: #10121c; !important;">' +data.body[i].snippets[j].answer + '</span>', leaf:true});
                }
        }
        //

        this.getTreePanel().setRootNode(this.root);
        //middleSection.update({'Title':data.Title, 'Paragraph':data.header[0].paragraph});


    },
    resetQA:function(){

        this.getTreePanel().collapseAll();
    },
    onShow:function(){

        var me=this;
        setTimeout(function(){
            var marg = Ext.getBody().getViewSize().height - 80 - 220 -90;// -  me.getStepbystepview().getComponent(1).getComponent(0).getHeight()/2;

            //  me.getStepbystepview().getComponent(1).setHeight(marg);

            me.getTreePanel().setHeight(marg);


        },10);

    }
});