Ext.define('ePatientChase.controller.Main', {
    extend: 'Ext.app.Controller',
    stores:['Reference','Flow'],
    refs:[
        {
            ref:'mainViewport',
            selector:'#mainViewport'
        },{
            ref:'searchTab',
            selector:'#searchTab'
        }

    ],
    root:null,
    snippetsToLoad:0,
    snippets:[],
    init: function() {

        this.control({
            '#mainViewport':{
                beforerender:'onShow'

            },
            '#searchField':{

                specialkey:'searchEnter'
            }
        });

        if( this.getReferenceStore() == null ||  this.getReferenceStore() == undefined)
            this.getReferenceStore().on('load',this.getSnippetsList,this);
        else
            this.getSnippetsList();

    },

    getSnippetsList:function(){
        var me = this;


        if( this.getReferenceStore().getAt(0) == undefined){
            setTimeout(function(){me.getSnippetsList();},200);
            return;
        }

        var data = this.getReferenceStore().getAt(0).raw;

        this.snippets = [];

        for(var o in data){
            // console.log(o + " " + data[o]);

            if(data[o] != "Reference" && o != "description")
            {
                //    alert(o);
        //        this.root.children.push({text:""+o  , leaf:false, id:""+o});


            //    var indx = this.root.children.length - 1;


                for(var s in data[o]){

                    var theSnip = this.getSnippet(data[o][s].id);

                    this.snippets.push({name:theSnip.name, summary:theSnip.summary, image:theSnip.image, FileName:theSnip.FileName, id:theSnip.id});
                    //    console.log(theSnip);
                    //    console.log(data[o][s]);
                }
            }
        }
        this.snippetsToLoad = this.snippets.length;
        this.loadAllSnippets();
    },

    loadAllSnippets:function(){

        console.log("SNIP SNIP SNIP SNIP SNIP SNIP SNIP SNIP SNIP SNIP ")
        for(var i = 0; i < this.snippetsToLoad; i++)
        this.loadSnippetData(this.snippets[i].id, this.snippets[i].FileName,this.snippets[i].name, this.snippets[i].summary ,  this.snippets[i].image);


    },  loadSnippetData:function(snippetID, fileName, theName, theSummary, theImage){
        var me = this;
        var snippetURL = ePatientChase.Utilities.baseURL +'Content/'+ snippetID + '/' + fileName;

        Ext.Ajax.request({
            url:snippetURL,

            success: function(response){
                var data =JSON.parse( response.responseText);
                data.id = snippetID;
                data.FileName = fileName;
                data.name =theName;
                data.image = theImage;
                data.summary = theSummary;
                console.log(data);
                me.snippetsToLoad--;

            //    if(me.snippetsToLoad <= 0)
           //     alert("LOADED");

                ePatientChase.Utilities.snippets.push(data)
                /*
                var data =JSON.parse( response.responseText);
                console.log(me.myView);
                var bottomSection = me.myView.child('bottomsectionview');
                bottomSection.addSnippet(data, snippetURL);*/
            },
            failure:function(response){
                alert('error');
            }
        });

    },
    getSnippet:function(guid){
        var flowData = this.getFlowStore().getAt(0).raw;
        var myGUID = guid;
        var mySnippet;

        this.traverse(flowData,function(key, value, par, parpar, parparpar){
            //     if(key.snippets != undefined)
            //  console.log(value + " " + myGUID);
            if(key == "id"  && value == myGUID){
                // console.log(value);
                mySnippet =  par;
                mySnippet.parent = parparpar.name;

            }
            //if(key.id != undefined)
            //   console.log(key + " : "+value);
        },undefined);

        return mySnippet;
    },
    traverse:function(o,func, parpar, parparpar) {
        for (var i in o) {
            func.apply(this,[i,o[i], o, parpar, parparpar]);
            if (typeof(o[i])=="object") {
                //going on step down in the object tree!!
                this.traverse(o[i],func, o, parpar);
            }
        }

    },
    onShow:function(){
     // alert(  this.getMainViewport());
        var me = this;
    //    me.getSearchTab().setVisible(false);
        me.getSearchTab().tab.hide();
        var getParams = document.URL.split("?");
        var params = Ext.urlDecode(getParams[getParams.length - 1]);
        var me = this;

        if(params.reference != undefined ){

            me.getMainViewport().getComponent(1).setActiveTab(4);
        }
        else if(params.print != undefined ){

            me.getMainViewport().removeAll();
          //  me.getMainViewport().add({xtype:'printview'});

        }

        //setActiveTab(2);
    },

    searchEnter:function(field, e){
        var me = this;
        if (e.getKey() == e.ENTER) {

        //    me.getMainViewport().getComponent(1).setActiveTab(4);
            ePatientChase.Utilities.searchedPhrase = field.value;
            me.getSearchTab().tab.show();

            if(  me.getMainViewport().getComponent(1).getActiveTab().xtype == 'searchview'){
            //      me.getSearchTab().setContent();
               me.getSearchTab().fireEvent("setContent");
            }

            else
                me.getMainViewport().getComponent(1).setActiveTab(5);
         //   alert(field.value);
        }
    }
});