Ext.define('ePatientChase.controller.Snippet', {
    extend: 'Ext.app.Controller',
    refs:[
        {
            ref:'snippetview',
            selector:'snippetview'

        }
    ],
    snippetData:null,
    snippetURL:null,
    init: function() {


            this.control({
            'snippetview': {
                'click': 'onSnippedClicked',
                'mouseover':'onSnippetMouseOver',
                'mouseout':'onSnippetMouseOut'

            }
        });
    },
    onSnippetMouseOver:function(me, e, eOpts){


        var snippets = Ext.ComponentQuery.query('snippetview');

        snippets.forEach(function(element, index, array){

          if( element.id != e.target.id ){
              element.fadeTo(0.5);
          }else{
           //   element.fadeTo(1);
          }
        });

        me.fadeTo(1);
    },
    onSnippetMouseOut:function(me, e, eOpts){
        var snippets = Ext.ComponentQuery.query('snippetview');

        snippets.forEach(function(element, index, array){


                element.fadeTo(1);

        });
    },
    onSnippedClicked:function(myself, e, eOpts ){
        var me = this;

        this.snippetData = myself.snippetData.snippet;
        this.snippetURL =myself.snippetURL ;
       var win1 =  Ext.create('Ext.window.Window', {
            title: '',
            height: '80%',
            width: 900,
            id:'contentWindow',
            layout: 'fit',
            autoScroll:true,
            modal:true,
            html:me.getSnippetHTML(),
            style:{'opacity':'0'},
           listeners:{
               focus:function(){
                   if(Ext.WindowManager.get('ytWindow') != undefined){
                        setTimeout(function(){
                           Ext.WindowManager.bringToFront ('ytWindow');
                            }, 100);
                   }

               },
               move:function(){
                   if(Ext.WindowManager.get('ytWindow') != undefined){
                       setTimeout(function(){
                           Ext.WindowManager.bringToFront ('ytWindow');
                       }, 300);
                   }

               }
           }

        }).show();

        win1.animate({
            easing:'easeIn',
            to: {
               opacity:1

            }
        });

        Ext.WindowManager.register (win1);
    },
    getURL:function(theURL){
        var urlBase = theURL.replace('../../', ePatientChase.Utilities.baseURL);

        if(urlBase.indexOf("http") == -1 && urlBase.indexOf("https")){
            urlBase = ePatientChase.Utilities.baseURL + urlBase;
        }

        return urlBase;

    },
    getSnippetHTML:function(){
        var data  = this.snippetData;
        var snippetURL =  this.snippetURL;

        var html = [
            '<div class="snippetContent" style="padding:10px">',
            '<h1>'+data.title +'</h1>'

        ];

        for(var i = 0 ; i < data.content.length; i++){
            if(data.content[i] == "section")
              html.push('<hr/>');
            else if(data.content[i].paragraph != null)
                html.push('<p>'+data.content[i].paragraph+'</p>');
            else if(data.content[i].image != null)
                html.push('<img src="'+ this.getURL(data.content[i].image) +'" class="contentImage"/>');
            else if(data.content[i].list != null&& data.content[i].list != undefined){
                var listHTML = [];
                listHTML.push('<h3>'+data.content[i].list+'</h3>');

                listHTML.push('<ul>');
                for(var l = 0; l < data.content[i].items.length; l++)
                    listHTML.push('<li>'+data.content[i].items[l]+'</li>');

                listHTML.push('</ul>');
                html.push(listHTML);
            }
            else if(data.content[i].link != null )
                html.push('<a href="'+ this.getURL(data.content[i].url)+'" class="snippetLink" target="_blank">'+data.content[i].link +'</a> ');
        }

        var me = this;
        var videoID =data.video.replace('http://www.youtube.com/watch?v=','');

        if(data.video != ""){

            var imageURL = 'http://img.youtube.com/vi/'+videoID+'/0.jpg';

            html.push('<div class="contentVideoHolder"    style="position: relative; width:420px; height:345px; margin-left: auto; margin-right: auto; display: block; cursor: pointer" >');
            html.push('<img src="'+ imageURL +'" class="contentImage" style="width:420px; height:345px;"/>');
            html.push('<img src="resources/play-button-red.png" class="contentYTImage" id="yt_'+videoID+'" style="width:420px; height:345px;"/>');
            html.push('</div>');

            setTimeout(function(){
            Ext.select('.contentVideoHolder').on('mousedown', function(e){
                var ytID = e.target.id.replace('yt_','');

                me.openYouTubeWindow(ytID);


            })}, 100);
            /*
            html.push('<iframe  class="contentVideo" width="420" height="345"'+
            'src="http://www.youtube.com/embed/'+videoID+'">'+
            '</iframe>');*/
        }

        html.push('</div>');
        return html;
    },
    openYouTubeWindow:function(videoID){
        var myHTML = '<iframe  class="contentVideo" width="100%" height="100%"'+
            'src="http://www.youtube.com/embed/'+videoID+'">';

        var win =  Ext.create('Ext.window.Window', {
            title: 'YouTube',
            height: '80%',
            width: 900,
            id:"ytWindow",
            layout: 'fit',
            autoScroll:true,
            modal:false,
            html:myHTML,

            style:{'opacity':'0'}

        }).showAt(0,0);

        win.animate({
            easing:'easeIn',
            to: {
                opacity:1

            }
        });

        Ext.WindowManager.register (win);
        Ext.WindowManager.get('contentWindow').add(win);

       // Ext.WindowManager.bringToFront ('ytWindow');
    }
});