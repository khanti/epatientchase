Ext.define('ePatientChase.controller.Reference', {
    extend: 'ePatientChase.controller.TabController',
    stores:['Reference','Flow'],
    id:'refController',
    refs:[
        {
            ref:'referenceview',
            selector:'referenceview'

        },{
            ref:'treePanel',
            selector:'#refTreePanel'

        },{
            ref:'carouselcontainer3',
            selector:'carouselcontainer3'
        }
    ],
    root:null,
    snippets:[],
    currentIndx:0,
    mySnippetID:undefined,
    currentRecord:null,
    init: function() {
     //   this.setMyView(this.getReferenceview());
     //   this.getReferenceStore().on('load',this.setContent,this);
        var me = this;
        this.setMyView(this.getReferenceview());

        this.control({
            '#refResetButton':{
                click:'resetRef'
            },
            '#refTreePanel':{
                itemmouseenter  :'loadSnippet',
                itemclick:'showSnippetOnClick'
            },
            'referenceview':{
                carouselItemClicked:'showSnippetOnClick',
                carouselItemChanged:'onItemChanged',
                show:'onShow',
                resetRef:'resetRef'
            }

        });
        var getParams = document.URL.split("?");
        var params = Ext.urlDecode(getParams[getParams.length - 1]);


        if(params.reference != undefined )
            this.mySnippetID = params.reference;


 
    },
    onShow:function(){
         this.onFlowLoad();

        if( Ext.getBody().getViewSize().height <= 760){
            this.getReferenceview().child('topsectionview').hide(true);
            this.getReferenceview().child('bottomsectionview').hide(true);

        }
    },
    onFlowLoad:function(){



        if( this.getReferenceStore() == null ||  this.getReferenceStore() == undefined)
            this.getReferenceStore().on('load',this.setContent,this);
        else
            this.setContent();
    },

    setContent:function(){

        var topSection    = this.getReferenceview().child('topsectionview');
        var middleSection = this.getReferenceview().child('middlesectionview');
        var bottomSection = this.getReferenceview().child('bottomsectionview');
        var me = this;


       if( this.getReferenceStore().getAt(0) == undefined){
           setTimeout(function(){me.setContent();},200);
           return;
       }
        var data = this.getReferenceStore().getAt(0).raw;
        topSection.update({'Title':data.name,'Description':data.description});

            this.root = new Object();

            this.root.text = "Root";
            this.root.expanded = false;
            this.root.children = new Array();


        this.snippets = [];

        for(var o in data){
           // console.log(o + " " + data[o]);

            if(data[o] != "Reference" && o != "description")
            {
            //    alert(o);
                this.root.children.push({text:""+o  , leaf:false, id:""+o});


                var indx = this.root.children.length - 1;

                this.root.children[indx].children = new Array();

                for(var s in data[o]){
                    this.root.children[indx].children.push({text:data[o][s].name  , leaf:true, id:data[o][s].id, name:data[o][s].name });

                   var theSnip = this.getSnippet(data[o][s].id);
                    if(   this.snippets.length == 0)
                    this.snippets.push({name:theSnip.name, summary:theSnip.summary, image:theSnip.image, FileName:theSnip.FileName, id:theSnip.id});
                    this.snippets.push({name:theSnip.name, summary:theSnip.summary, image:theSnip.image, FileName:theSnip.FileName, id:theSnip.id});
                //    console.log(theSnip);
                //    console.log(data[o][s]);
                }
            }
        }

        this.getTreePanel().setRootNode(this.root);


        setTimeout(function(){  me.getCarouselcontainer3().setCarouselData( me.snippets, 0);},1000);



      //  me.getTreePanel().expandPath('/root//'+o);
        if( this.mySnippetID != undefined){

            var i;

            for( i in this.snippets){
                if(this.snippets[i].id ==  this.mySnippetID )
                {   this.currentIndx = i;
                    me.getTreePanel().selectPath( me.getTreePanel().getStore().getNodeById(this.mySnippetID).getPath());

                    GLOB.setSlide(i);

               //     this.getTreePanel().expandNode(  this.getTreePanel().root);
               //   this.getTreePanel().selectPath('/root//'+this.mySnippetID+"/");
              //      this.getTreePanel().selectPath('/root//sww'+this.mySnippetID+"/");

                    //      alert(i);
                    return;

                    //   this.toggleSnippet(i);
                }
            }

            this.currentIndx = -1;

        }
/*
        middleSection.tpl =new Ext.XTemplate(
            '<h1>{Title}</h1>',
            '<p class="sectionHeader">{Paragraph}</p>'
        );
 //71929e71a6f9402e953e8b269abd91e6
        middleSection.update({'Title':data.Title, 'Paragraph':data.header[0].paragraph});
*/


    },
    onItemChanged:function(a){
        //alert(a.itemIndx);
      this.currentIndx   = a.itemIndx;
    },
    addSnippet:function(){

    },
    resetRef:function(){
        this.getTreePanel().collapseAll();
    },
    getSnippet:function(guid){
        var flowData = this.getFlowStore().getAt(0).raw;
        var myGUID = guid;
        var mySnippet;

        this.traverse(flowData,function(key, value, par, parpar, parparpar){
            //     if(key.snippets != undefined)
            //  console.log(value + " " + myGUID);
            if(key == "id"  && value == myGUID){
                // console.log(value);
                mySnippet =  par;
                mySnippet.parent = parparpar.name;

            }
            //if(key.id != undefined)
            //   console.log(key + " : "+value);
        },undefined);

        return mySnippet;
    },
    traverse:function(o,func, parpar, parparpar) {
        for (var i in o) {
            func.apply(this,[i,o[i], o, parpar, parparpar]);
            if (typeof(o[i])=="object") {
                //going on step down in the object tree!!
                this.traverse(o[i],func, o, parpar);
            }
        }

    },
    loadSnippet:function ( me,record, item, index, e, eOpts ){

        var i;
        if(record.raw.id != undefined){
            for( i in this.snippets){
                if(this.snippets[i].id == record.raw.id )
                {
                    if(this.currentIndx == i) return;
                    this.currentIndx = i;

                    if(i == 0)
                        this.currentIndx = 1;
                    this.currentRecord = record;
                     console.log("sliede " + this.currentIndx);

                    GLOB.setSlide(this.currentIndx);
              //      alert(i);
                    return;

                    //   this.toggleSnippet(i);
                }
            }
        }

        this.currentRecord = null;
        this.currentIndx = -1;
    },
    itemClicked:function(me, record, item, index, e, eOpts){

    },
    showSnippetOnClick:function(me, record, item, index, e, eOpts){
    //    alert(record.getPath() );return;
        if( this.currentIndx >= 0 )
            this.showSnippet(this.snippets[this.currentIndx].id, this.snippets[this.currentIndx].FileName);
    }
});