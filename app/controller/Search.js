Ext.define('ePatientChase.controller.Search', {
    extend: 'ePatientChase.controller.TabController',
    stores:['Search','Flow'],
    id:'refController',
    refs:[
        {
            ref:'searchview',
            selector:'searchview'

        },{
            ref:'treePanel',
            selector:'#refTreePanel2'

        },{
            ref:'carouselcontainer4',
            selector:'carouselcontainer4'
        }
    ],
    root:null,
    snippets:[],
    currentIndx:0,
    mySnippetID:undefined,
    currentRecord:null,
    currentIndx:0,
    searchResults:[],
    snippetsAdded:null,
    init: function() {
     //   this.setMyView(this.getSearchview());
     //   this.getSearchStore().on('load',this.setContent,this);
        var me = this;
        this.setMyView(this.getSearchview());
        this.snippetsAdded = new Array();
        this.control({
            '#refResetButton':{
                click:'resetRef'
            },
            '#refTreePanel2':{
                itemmouseenter  :'loadSnippet',
                checkchange:'oncheck',
                itemclick:'itemClicked'
          //      itemclick:'showSnippetOnClick'
            },
            'searchview':{
                carouselItemClicked:'showSnippetOnClick',
                carouselItemChanged:'onItemChanged',
                show:'onShow',
                resetRef:'resetRef',
                setContent:'setContent'
            },
            '#searchStoryBoardButton':{
                click:'addBookmark'
            }

        });
        var getParams = document.URL.split("?");
        var params = Ext.urlDecode(getParams[getParams.length - 1]);


        if(params.search != undefined )
            this.mySnippetID = params.search;


    },
    onShow:function(){
        this.snippetsAdded = [];
        //this.clearSnippets();
         this.onFlowLoad();
    },
    onFlowLoad:function(){



        if( this.getSearchStore() == null ||  this.getSearchStore() == undefined)
            this.getSearchStore().on('load',this.setContent,this);
        else
            this.setContent();
    },

    setContent:function(){
        this.setMyView(this.getSearchview());
        this.clearSnippets();
        this.currentIndx = 0;
        var topSection    = this.getSearchview().child('topsectionview');
        var middleSection = this.getSearchview().child('middlesectionview');
        var bottomSection = this.getSearchview().child('bottomsectionview');
        var me = this;


       if( this.getSearchStore().getAt(0) == undefined){
           setTimeout(function(){me.setContent();},200);
           return;
       }
        var data = this.getSearchStore().getAt(0).raw;
        topSection.update({'Title':data.Title,'Description':data.description + " <b>" +  ePatientChase.Utilities.searchedPhrase  + "</b>"});

            this.root = new Object();

            this.root.text = "Root";
            this.root.expanded = false;
            this.root.children = new Array();

            this.snippets = [];

        var results = this.filterSnippets();
        this.searchResults = results;

        for(var i=0; i <results.length; i++)

            this.root.children.push({text: this.searchResults[i].name  , leaf:true, id: this.searchResults[i].id, checked:false});




        this.getTreePanel().setRootNode(this.root);
        if(results.length > 0){
            results.unshift(results[0]);
            setTimeout(function(){  me.getCarouselcontainer4().setCarouselData( results, 0);},1000);
        }





    },
    filterSnippets:function(){
        var results = [];

        for(var i = 0; i <ePatientChase.Utilities.snippets.length; i++ ){
            if(this.checkForPhrase(ePatientChase.Utilities.snippets[i])){

                    results.push(ePatientChase.Utilities.snippets[i]);

            }

        }

        return results;

    },
    checkForPhrase:function(snip){
        var phrase = ePatientChase.Utilities.searchedPhrase;

        console.log(snip);

        if(this.containsPhrase(snip.name))            return true;
        if(this.containsPhrase(snip.snippet.title))            return true;
        if(this.containsPhrase(snip.snippet.summary))            return true;

        for(var i = 0; i < snip.snippet.content.length; i++){
console.log(snip.snippet.content[i]);
            if(snip.snippet.content[i].paragraph != undefined && this.containsPhrase(snip.snippet.content[i].paragraph) )
             return true;
        //    else if(snip.snippet.content[i].link != undefined && this.containsPhrase(snip.snippet.content[i].link) )
          //      return true;
            else if(snip.snippet.content[i].list != undefined && this.containsPhrase(snip.snippet.content[i].list) )
                return true;
            else if(snip.snippet.content[i].items != undefined   ){
                for(var it in snip.snippet.content[i].items){
                    if(this.containsPhrase(snip.snippet.content[i].items[it]))
                    return true;
                }
            }

        }


        return false;
    },
    containsPhrase:function(theString){
        if(theString == undefined) return false;
        theString = theString.toLowerCase ();
        var phrase = ePatientChase.Utilities.searchedPhrase.toLowerCase ();

        if(theString.indexOf(phrase) != -1) return true;

        return false;

    },

    onItemChanged:function(a){
        //alert(a.itemIndx);
      this.currentIndx   = a.itemIndx;
    },

    addBookmark:function(){
        var mySnippetsID = [];
        var mySnippetsFileName = [];

        Ext.MessageBox.buttonText.ok = "Yes";
        Ext.MessageBox.buttonText.cancel = "No";

        for(var i = 0; i < this.snippetsAdded.length; i++){


                mySnippetsID.push(this.searchResults[this.snippetsAdded[i]].id);
                mySnippetsFileName.push(this.searchResults[this.snippetsAdded[i]].FileName);

        }

        Ext.Msg.prompt('Create Storyboard containing search term?', 'Bookmark name:', function(btn, text){
            if (btn == 'ok'){

                var Bookmark = StackMob.Model.extend({ schemaName: 'bookmark' });
                var myBookmark = new Bookmark({bookmark_name:text, snippets_id:mySnippetsID, snippets_filename:mySnippetsFileName});

                myBookmark.create({
                    success: function(model) {

                        var theID = model.get('bookmark_id');
                        Ext.getStore('Bookmarks').add({name:text, snippets_id:mySnippetsID, snippets_filename:mySnippetsFileName, url:theID});
                        Ext.getStore('Bookmarks').sync();
                    },
                    error: function(model, response) {
                        Ext.Msg.alert('Sorry', 'Cannot connect to the server. Bookmark will be stored locally.');
                        Ext.getStore('Bookmarks').add({name:text, snippets_id:mySnippetsID, snippets_filename:mySnippetsFileName, url:'off line'});
                        Ext.getStore('Bookmarks').sync();
                    }
                });
            }
        });

        this.createStoryBoard();
    },
    resetRef:function(){
        this.getTreePanel().collapseAll();
    },
    getSnippet:function(guid){
        var flowData = this.getFlowStore().getAt(0).raw;
        var myGUID = guid;
        var mySnippet;

        this.traverse(flowData,function(key, value, par, parpar, parparpar){
            //     if(key.snippets != undefined)
            //  console.log(value + " " + myGUID);
            if(key == "id"  && value == myGUID){
                // console.log(value);
                mySnippet =  par;
                mySnippet.parent = parparpar.name;

            }
            //if(key.id != undefined)
            //   console.log(key + " : "+value);
        },undefined);

        return mySnippet;
    },
    traverse:function(o,func, parpar, parparpar) {
        for (var i in o) {
            func.apply(this,[i,o[i], o, parpar, parparpar]);
            if (typeof(o[i])=="object") {
                //going on step down in the object tree!!
                this.traverse(o[i],func, o, parpar);
            }
        }

    },
    loadSnippet:function ( me,record, item, index, e, eOpts ){

        var i;
        if(record.raw.id != undefined){
            for( i in this.searchResults){
                if(this.searchResults[i].id == record.raw.id )
                {
                    if(this.currentIndx == i) return;
                    this.currentIndx = i;


                    console.log(i);

                     //   alert(this.currentIndx);
                   if(i == 0)
                       this.currentIndx = 1;
                    this.currentRecord = record;
                    GLOB.setSlide(this.currentIndx);

              //      alert(i);
                    return;

                    //   this.toggleSnippet(i);
                }
            }
        }

        this.currentRecord = null;
        this.currentIndx = -1;
    },
    bIgnoreItemClick:false,
    fireItemClick:function(me, record, item, index, e, eOpts){
        if(!this.bIgnoreItemClick)
        {
            if( this.currentIndx >= 0 )
                this.showSnippet(this.searchResults[this.currentIndx].id, this.searchResults[this.currentIndx].FileName);
        }

        this.bIgnoreItemClick = false;
    }
    ,
    showSnippetOnClick:function(newIndxme){
        //    alert(record.getPath() );return;
       // alert("hej");
        //console.log("click ");
      //  console.log(me);
       //var me2 = this; me2.fireItemClick(me, record, item, index, e, eOpts);
     //   this.currentIndx = newIndxme.itemIndx;
        this.showSnippet(this.searchResults[this.currentIndx].id, this.searchResults[this.currentIndx].FileName);
        setTimeout(function(){

        },1);
    },
    itemClicked:function(me, record, item, index, e, eOpts){

        if(record.childNodes.length == 0){
            this.oncheck(record,!record.get('checked'), eOpts );
            record.set('checked',!record.get('checked'));

        }
    }
,
    oncheck:function(node, checked, eOpts){
        this.bIgnoreItemClick = true;
        if(node.data.id != undefined){
            for(var i in this.searchResults){
                if(this.searchResults[i].id == node.data.id )
                {      this.toggleSnippet(parseInt(i));
                    return;
                }
            }
        }


    },
    toggleSnippet:function(indx){
        //if(indx != 1) indx++;
    //      alert(indx + " --  " + this.snippetsAdded(indx));
    if(this.snippetsAdded.indexOf(indx) == -1)
        this.snippetsAdded.push(indx);
    else
        this.snippetsAdded.splice(this.snippetsAdded.indexOf(indx) , 1);
    ///  this.snippetsAdded.push(indx);
    //   alert(this.snippetsAdded.length);

    },
    createStoryBoard:function(){


        var me = this;
        me.clearSnippets();
        me.dataidCache = [];
        me.dataFileNameCache = [];

        for(var i = 0; i < this.snippetsAdded.length; i++){
           // console.log(i + " bi bi bi ");
         //   if(this.snippetsAdded.indexOf(""+i) != -1){
           //     console.log(this.searchResults[i].id, this.searchResults[i].FileName);
            me.dataidCache.push(this.searchResults[ this.snippetsAdded[i] ].id);
            me.dataFileNameCache.push( this.searchResults[this.snippetsAdded[i] ].FileName);

          //     this.addSnippet(this.searchResults[ this.snippetsAdded[i] ].id, this.searchResults[this.snippetsAdded[i] ].FileName);
           // }
        }
      //  this.addBookmark();
        this.showStory(me.dataidCache, me.dataFileNameCache );
    },

    dataidCache:[],
    dataFileNameCache:[],
    showStory:function(dataid, dataFileName){

        var me = this;
        me.dataidCache = dataid;
        me.dataFileNameCache = dataFileName;
        this.clearSnippets();


        for(var s in dataid){
            setTimeout(function(){
                me.popSnippetToShow();

            }, (s+1)*50);

        }

    },
    popSnippetToShow:function(){
        var me = this;


        this.addSnippet(  me.dataidCache[0], me.dataFileNameCache[0]);
        me.dataidCache.splice(0,1);  me.dataFileNameCache.splice(0,1);
    },


});