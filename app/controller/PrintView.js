Ext.define('ePatientChase.controller.PrintView', {
    extend: 'ePatientChase.controller.TabController',
    stores:['Flow'],
    dataidCache:[],
    dataFileNameCache:[],
    refs:[
       /* {
            ref:'printview',
            selector:'printview'

        },
        {            ref:'printHtml',
            selector:'#printHTML'

        }
        */
    ],
    myHTML:"",
    init: function() {
   //     this.getFlowStore().on('load', this.loadBookmarks,this);
        var getParams = document.URL.split("?");
        var params = Ext.urlDecode(getParams[getParams.length - 1]);

        var me = this;
        if(params.print != undefined){
            //  alert(params.bookmark);

            var Bookmark = StackMob.Model.extend({ schemaName: 'bookmark' });
            var Bookmarks = StackMob.Collection.extend({ model: Bookmark });
            var myBookmarks = new Bookmarks();


            var q = new StackMob.Collection.Query();
            q.equals('bookmark_id', params.print  );

            myBookmarks.query(q, {
                success: function(model) {
                    // console.debug(model.toJSON()); //JSON array of matching Todo objects
                    var b = model.toJSON();

              ///      Ext.getStore('Bookmarks').add({name:b[0].bookmark_name, snippets_id:b[0].snippets_id, snippets_filename:b[0].snippets_filename, url:b[0].bookmark_id});
                //    Ext.getStore('Bookmarks').sync();

                    // console.log(b[0].bookmark_name)
                  //  Ext.Msg.alert('Success', 'Bookmark imported! ' );
                    me.setContent(b[0].bookmark_name,  b[0].snippets_id,  b[0].snippets_filename, b[0].bookmark_id);
                },
                error: function(model, response) {

                    Ext.Msg.alert('Sorry!', 'Bookmark not found');
                }
            });


         //  setTimeout(function(){  window.print();},100);
            this.bBookmarkImported = true;
        }




    },
    setContent:function(bookmark_name, snippets_id,snippets_filename, bookmark_id){

       var myHTML = bookmark_name + " " + snippets_id + " " + snippets_filename + " " + bookmark_id;
       this.showStory(snippets_id,snippets_filename );
    //    for(var i =0 ; i < snippets_id.length; i++)
      //    this.loadSnippetData(snippets_id[i], snippets_filename[i]);



     //  this.getPrintHtml().update({html:myHTML});
    },
    showStory:function(dataid, dataFileName){

        var me = this;
        me.dataidCache = dataid;
        me.dataFileNameCache = dataFileName;
     //   this.clearSnippets();


        for(var s in dataid){
           // alert(s);
            setTimeout(function(){
                me.popSnippetToShow();
          //      alert(s+1);
            }, (s+1)*30);

        }

    },
    popSnippetToShow:function(){
        var me = this;


        this.loadSnippetData(  me.dataidCache[0], me.dataFileNameCache[0]);
        me.dataidCache.splice(0,1);  me.dataFileNameCache.splice(0,1);

        if(   me.dataidCache.length == 0)
            setTimeout(function(){  window.print();},100);
    },
    loadSnippetData:function(snippetID, fileName){

        var snippetURL = ePatientChase.Utilities.baseURL +'Content/' + snippetID + '/' + fileName;
        var me = this;
    //    alert("snippetURL" + snippetURL);

        Ext.Ajax.request({
            url:snippetURL,

            success: function(response){

                var data =JSON.parse( response.responseText);

            //    console.log(data);



                //         var bottomSection = me.myView.child('bottomsectionview');
                //    bottomSection.addSnippet(data, snippetURL);
                var htmlData = me.getSnippetHTML2Print(data.snippet);
                var html = "";

                for(var h in htmlData){
                    if(htmlData[h].length > 0)
                    {
                        for(var h2 in htmlData[h])
                            html += htmlData[h][h2];
                    }
                    else
                        html += htmlData[h];
                }

         //    alert(html.join());
                me.myHTML +=   html;
                jQuery('body').html(me.myHTML);
                jQuery('*').css({'background-color':'white','overflow':'auto','height':'auto'});
           //     me.getPrintHtml().update({html:me.myHTML});
              //  me.getPrintHtml().update({html:'html'});



            },
            failure:function(response){
             //   alert('error');
            }
        });
    },

    getURL2Print:function(theURL){
        var urlBase = theURL.replace('../../', ePatientChase.Utilities.baseURL);

        if(urlBase.indexOf("http") == -1 && urlBase.indexOf("https")){
            urlBase = ePatientChase.Utilities.baseURL + urlBase;
        }

        return urlBase;

    },
    getSnippetHTML2Print:function(data){


        var html = [
            '<div class="snippetContent" style="padding:0px">',
            '<h1>'+data.title +'</h1>'

        ];


        for(var i = 0 ; i < data.content.length; i++){
            if(data.content[i] == "section")
                html.push('<hr/>');
            else if(data.content[i].paragraph != null)
                html.push('<p>'+data.content[i].paragraph+'</p>');
            else if(data.content[i].image != null)
                html.push('<img src="'+ this.getURL(data.content[i].image) +'" class="contentImage"/>');
            else if(data.content[i].list != null&& data.content[i].list != undefined){
                var listHTML = [];
                listHTML.push('<h3>'+data.content[i].list+'</h3>');

                listHTML.push('<ul>');
                for(var l = 0; l < data.content[i].items.length; l++)
                    listHTML.push('<li>'+data.content[i].items[l]+'</li>');

                listHTML.push('</ul>');
                html.push(listHTML);
            }
            else if(data.content[i].link != null )
                html.push('<a href="'+ this.getURL(data.content[i].url)+'" class="snippetLink" target="_blank">'+data.content[i].link +'</a> ');
        }

        var me = this;
        var videoID =data.video.replace('http://www.youtube.com/watch?v=','');

        if(data.video != ""){

            /*
            var imageURL = 'http://img.youtube.com/vi/'+videoID+'/0.jpg';

            html.push('<div class="contentVideoHolder"    style="position: relative; width:440px; height:390px; margin-left: auto; margin-right: auto; display: block; cursor: pointer" >');
            html.push('<img src="'+ imageURL +'" class="contentImage" style="width:420px; height:340px;"/>');
        //    html.push('<img src="resources/play-button-red.png" class="contentYTImage" id="yt_'+videoID+'" style="width:430px; height:300px;"/>');
            html.push('</div>');

            setTimeout(function(){
                Ext.select('.contentVideoHolder').on('mousedown', function(e){
                    var ytID = e.target.id.replace('yt_','');

                    me.openYouTubeWindow(ytID);


                })}, 100);
                */
            /*
             html.push('<iframe  class="contentVideo" width="420" height="345"'+
             'src="http://www.youtube.com/embed/'+videoID+'">'+
             '</iframe>');*/
        }

        html.push('</div>');
        return html;
    }

});