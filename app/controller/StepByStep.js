Ext.define('ePatientChase.controller.StepByStep', {
    extend: 'ePatientChase.controller.TabController',
    stores:['StepByStep','Flow'],

    refs:[
        {
            ref:'stepbystepview',
            selector:'stepbystepview'

        },{
            ref:'carouselcontainer2',
            selector:'carouselcontainer2'
        }
    ],
    root:null,
    snippets:null,
    currentIndx:1,
    snippetsAdded:null,

    init: function() {

        this.snippetsAdded = new Array();

        this.control({
            'stepbystepview':{
                show:'onShow',
                'carouselItemClicked':'carouselItemClicked',
                resetStoryboard:'resetStoryboard'
            },
            '#stepbystepStoryBoardButton': {
                click: 'createStoryBoard'
            },
           '#stepbystepResetdButton':{
                click:'resetStoryboard'
             },
            '#advancedTreePanel':{
                itemmouseenter  :'loadSnippet',
                checkchange:'oncheck',
                itemclick:'itemClicked'
            }

        });

        this.getFlowStore().on('load',this.onFlowLoad,this);
     //   this.getStepByStepStore().on('load',this.setContent,this);

    },
    onShow:function(){
        var me=this;

        if( Ext.getBody().getViewSize().height <= 760){
            this.getStepbystepview().child('topsectionview').hide(true);
            this.getStepbystepview().child('bottomsectionview').hide(true);

        }
else
        setTimeout(function(){
            var marg = Ext.getBody().getViewSize().height - 80 - 220 -90;// -  me.getStepbystepview().getComponent(1).getComponent(0).getHeight()/2;

           //  me.getStepbystepview().getComponent(1).setHeight(marg);
            marg = marg/2 -408/2;
            me.getStepbystepview().getComponent(1).setMargin(marg + " 0 0 0");
            me.getStepbystepview().getComponent(1).setHeight(2000);
      //     alert();

        },100);

       this.getCarouselcontainer2().setCarouselData( this.snippets, 0);
    },
    carouselItemClicked:function(){
     //   alert();

        this.showSnippet(this.snippets[this.currentIndx].id, this.snippets[this.currentIndx].FileName);
         //   this.showSnippet(this.HotText[this.currentStep][this.SentenceOptions[this.currentStep]].id,
          //      this.HotText[this.currentStep][this.SentenceOptions[this.currentStep]].FileName);
    },
    loadSnippet:function ( me,record, item, index, e, eOpts ){
        var i;
        if(record.raw.id != undefined){
            for( i in this.snippets){
                if(this.snippets[i].id == record.raw.id  )
                {
                    if(this.currentIndx == i) return;
                    this.currentIndx = i;
                    GLOB.setSlide(i);
                    return;
                    break;
                 //   this.toggleSnippet(i);
                }
            }
        }

    },
    itemClicked:function(me, record, item, index, e, eOpts){

        if(record.childNodes.length == 0){
           this.oncheck(record,!record.get('checked'), eOpts );
             record.set('checked',!record.get('checked'));

        }
    },
    bIgnoreItemClick:false,
    fireItemClick:function(me, record, item, index, e, eOpts){
        if(!this.bIgnoreItemClick)
        {
            if( this.currentIndx >= 0 )
                this.showSnippet(this.snippets[this.currentIndx].id, this.snippets[this.currentIndx].FileName);
        }

        this.bIgnoreItemClick = false;
    },
    oncheck:function(node, checked, eOpts){
        this.bIgnoreItemClick = true;

        if(node.data.id != undefined){
            for(var i in this.snippets){
                if(this.snippets[i].id == node.data.id )
                {      this.toggleSnippet(i);
                    return;
                }
            }
        }


    }
    ,    showSnippetOnClick:function(me, record, item, index, e, eOpts){
    //    alert(record.getPath() );return;
        var me2 = this;
        setTimeout(function(){
            me2.fireItemClick(me, record, item, index, e, eOpts);
        },100);
},
    toggleSnippet:function(indx){
  //      alert(indx + " --  " + this.snippetsAdded(indx));
        if(this.snippetsAdded.indexOf(indx) == -1)
            this.snippetsAdded.push(indx);
        else
            this.snippetsAdded.splice(this.snippetsAdded.indexOf(indx) , 1);
      ///  this.snippetsAdded.push(indx);
     //   alert(this.snippetsAdded.length);

    },
    onFlowLoad:function(){
        this.setMyView(this.getStepbystepview());

        if( this.getStepByStepStore() == null ||  this.getStepByStepStore() == undefined)
            this.getStepByStepStore().on('load',this.setContent,this);
        else
            this.setContent();
    },
    createStoryBoard:function(){
        var me = this;
        me.clearSnippets();
        me.dataidCache = [];
        me.dataFileNameCache = [];

        for(var i = 0; i < this.snippets.length; i++){
            console.log(i + " ");
            if(this.snippetsAdded.indexOf(""+i) != -1){

                me.dataidCache.push(this.snippets[i].id);
                me.dataFileNameCache.push(this.snippets[i].FileName);

          //  this.addSnippet(this.snippets[i].id, this.snippets[i].FileName);
            }
        }
       this.addBookmark();
        this.showStory(me.dataidCache, me.dataFileNameCache );
    },
    dataidCache:[],
    dataFileNameCache:[],
    showStory:function(dataid, dataFileName){

        var me = this;
        me.dataidCache = dataid;
        me.dataFileNameCache = dataFileName;
        this.clearSnippets();


        for(var s in dataid){
            setTimeout(function(){
                me.popSnippetToShow();

            }, (s+1)*30);

        }

    },
    popSnippetToShow:function(){
        var me = this;


        this.addSnippet(  me.dataidCache[0], me.dataFileNameCache[0]);
        me.dataidCache.splice(0,1);  me.dataFileNameCache.splice(0,1);
    },
    addBookmark:function(){
        var mySnippetsID = [];
        var mySnippetsFileName = [];

        Ext.MessageBox.buttonText.ok = "Yes";
        Ext.MessageBox.buttonText.cancel = "No";

        for(var i = 0; i < this.snippets.length; i++){

            if(this.snippetsAdded.indexOf(""+i) != -1){
                mySnippetsID.push(this.snippets[i].id);
                mySnippetsFileName.push(this.snippets[i].FileName);
            }
        }

        Ext.Msg.prompt('Bookmark Storyboard?', 'Bookmark name:', function(btn, text){
            if (btn == 'ok'){

                var Bookmark = StackMob.Model.extend({ schemaName: 'bookmark' });
                var myBookmark = new Bookmark({bookmark_name:text, snippets_id:mySnippetsID, snippets_filename:mySnippetsFileName});

                myBookmark.create({
                    success: function(model) {

                        var theID = model.get('bookmark_id');
                        Ext.getStore('Bookmarks').add({name:text, snippets_id:mySnippetsID, snippets_filename:mySnippetsFileName, url:theID});
                        Ext.getStore('Bookmarks').sync();
                    },
                    error: function(model, response) {
                        Ext.Msg.alert('Sorry', 'Cannot connect to the server. Bookmark will be stored locally.');
                        Ext.getStore('Bookmarks').add({name:text, snippets_id:mySnippetsID, snippets_filename:mySnippetsFileName, url:'off line'});
                        Ext.getStore('Bookmarks').sync();
                    }
                });
            }
        });
    },
    resetStoryboard:function(){
        this.clearSnippets();
        this.snippetsAdded.splice(this.snippetsAdded.length);
        this.resetToggle( this.getStepbystepview().child('middlesectionview').getComponent(0).getComponent(0).getRootNode() );
        this.getStepbystepview().child('middlesectionview').getComponent(0).getComponent(0).collapseAll();
    },
    resetToggle:function(node){

        for(var child in node.childNodes){
            if( node.childNodes[child].childNodes.length == 0 ){
                console.log( node.childNodes[child]);
                node.childNodes[child].data.checked = false;
            }

            else
           this.resetToggle(node.childNodes[child]);
        }
    },
    setContent:function(){
        var me = this;

        if(this.getStepByStepStore().getAt(0) == undefined){
            setTimeout(function(){me.setContent();},200);
            return;
        }

        var topSection    = this.getStepbystepview().child('topsectionview');
        var middleSection = this.getStepbystepview().child('middlesectionview');
        var bottomSection = this.getStepbystepview().child('bottomsectionview');




      var data = this.getStepByStepStore().getAt(0).raw;
       // console.log("#####");
        topSection.update({'Title':data.Title,'Description':data.description});
      //  console.log(this.getStepByStepStore().getAt(0).raw);

        middleSection.tpl =new Ext.XTemplate(
            '<h1>{Title}</h1>',
            '<p class="sectionHeader">{Paragraph}</p>'
        );

     //   middleSection.update({'Title':data.Title, 'Paragraph':data.header[0].paragraph});
  //      console.log(this.getFlowStore().getAt(0).raw);
        this.snippets = new Array();

        if(this.root == null){
            this.root = new Object();

            this.root.text = "Sections";
            this.root.expanded = false;
            this.root.children = new Array();
        }
        this.parseFlowData(this.getFlowStore().getAt(0).raw, this.root);

        console.log(this.root);
        this.getStepbystepview().child('middlesectionview').getComponent(0).getComponent(0).setRootNode(this.root);

        console.log("snips!");
        console.log(this.snippets);

    },

    parseFlowData:function(data, node){
        console.log("**********");
        console.log(data);
        console.log(":");

        for(var d in data){

            console.log(data[d]);

            if( data[d].name != undefined && data[d].name != "" && data[d].snippets != undefined)  {

                node.children.push({text:data[d].name, leaf:false, children:[]});
                var indx = node.children.length - 1;
              //  this.root.children[indx].children = new Array();

                for(var d2= 0; d2 < data[d].snippets.length; d2++){
                    if(data[d].snippets[d2].snippets == undefined){
                    node.children[indx].children.push({text:data[d].snippets[d2].name, leaf:true, checked:false, parent:    data[d].name,id:data[d].snippets[d2].id});
                        if(   this.snippets.length == 0)
                            this.snippets.push({name:data[d].snippets[d2].name, summary:data[d].snippets[d2].summary,
                                image:data[d].snippets[d2].image});

                    this.snippets.push({name:data[d].snippets[d2].name, summary:data[d].snippets[d2].summary,
                        image:data[d].snippets[d2].image,id:data[d].snippets[d2].id,
                        FileName:data[d].snippets[d2].FileName});


                    }
                    else{
                     //   node.children.push({text:d, leaf:false, children:[]});
               //         console.log("ADD JUST");
                  //      console.log(data[d].snippets[d2]);

                        this.parseFlowData([ data[d].snippets[d2]],  node.children[indx]);
                    }

                }



            }
            else if (typeof(data[d])=="object")
            {console.log(">>>>>>>>>");
                console.log(data[d]);
                node.children.push({text:d, leaf:false, children:[]});
                var indx = node.children.length - 1;
               this.parseFlowData(data[d],  node.children[indx]);

            }
            //console.log(d + " " + data[d] + " " +data[d].length);
        }



    }
});