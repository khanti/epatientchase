Ext.define('ePatientChase.controller.Home', {
    extend: 'ePatientChase.controller.TabController',
    stores:['Home','Bookmarks'],

    refs:[
        {
            ref:'homeview',
            selector:'homeview'

        }
    ],

    init: function() {
        this.getBookmarksStore().on('load', this.loadBookmarks,this);
        this.control({
            '#testbutton': {
                click: 'setContent'
            },
            'homeview':{
                show:'setContent',
                added:'setSize'
            },
            '#showIDButton':{
                click:'showID'
            }
        });


    },
    bBookmarkImported:false,
    loadBookmarks:function(){
        var getParams = document.URL.split("?");
        var params = Ext.urlDecode(getParams[getParams.length - 1]);
        var me = this;
        if(params.bookmark != undefined && this.bBookmarkImported == false){
          //  alert(params.bookmark);

            var Bookmark = StackMob.Model.extend({ schemaName: 'bookmark' });
            var Bookmarks = StackMob.Collection.extend({ model: Bookmark });
            var myBookmarks = new Bookmarks();


            var q = new StackMob.Collection.Query();
            q.equals('bookmark_id', params.bookmark  );

            myBookmarks.query(q, {
                success: function(model) {
                   // console.debug(model.toJSON()); //JSON array of matching Todo objects
                    var b = model.toJSON();

                    Ext.getStore('Bookmarks').add({name:b[0].bookmark_name, snippets_id:b[0].snippets_id, snippets_filename:b[0].snippets_filename, url:b[0].bookmark_id});
                    Ext.getStore('Bookmarks').sync();

                   // console.log(b[0].bookmark_name)
                    Ext.Msg.alert('Success', 'Bookmark imported! ' );
                    me.setContent();
                },
                error: function(model, response) {

                    Ext.Msg.alert('Sorry!', 'Bookmark not found');
                }
            });



            this.bBookmarkImported = true;
        }
        this.getHomeStore().on('load',this.setContent,this);
    },
    bookmarksInRow : 5,
    setSize:function(){
        var me=this;
        setTimeout(function(){
            var marg = Ext.getBody().getViewSize().height - 80 - 220 -170;// -  me.getStepbystepview().getComponent(1).getComponent(0).getHeight()/2;

            //  me.getStepbystepview().getComponent(1).setHeight(marg);
      //      marg = marg/2 -408/2;
          //  me.getHomeview().getComponent(1).setMargin(marg + " 0 0 0");
            //me.getHomeview().getComponent(1).setHeight(2000);
         //   alert(marg);
           me.getHomeview().getComponent(1).getComponent(1).setHeight(marg);
        me.bookmarksInRow = parseInt( marg/45 );

        },100);
    },
    setContent:function(){



        this.setMyView(this.getHomeview());

        var me = this;
        var topSection    = this.getHomeview().child('topsectionview');
        var middleSection = this.getHomeview().child('middlesectionview');

        middleSection.getComponent(1).removeAll();
        var data = this.getHomeStore().getAt(0).raw;

        topSection.update({'Title':data.Title,'Description':data.description});


        middleSection.getComponent(0).tpl =new Ext.XTemplate(

            '<p class="sectionHeader">{Paragraph}</p>'
        );

        middleSection.getComponent(0).update({'Title':data.Title, 'Paragraph':data.header[0].paragraph});


        var bX = 0, bY = -30, bIndx = 0;
        var bookmarks= this.getBookmarksStore();
        for(var i = 0; i < bookmarks.getCount(); i++){
            var cssText ={'font-size':'0.8em' , 'display':'inline-block !important','clear':'both', 'float':'left !important','position':'relative'};
        //   if( i >= 4)
          //  cssText= {'font-size':'0.8em' , 'display':'inline-block !important', 'float':'left !important','position':'relative'}

            bY += 45;
            bIndx++;
            if(bIndx > me.bookmarksInRow){
                bX += 320;
                bY = 15;
                bIndx = 0;
            }
            middleSection.getComponent(1).add(  {
                xtype:'button',
                text:bookmarks.getAt(i).raw.name ,
                bookmarkIndex:i,
                tooltip : bookmarks.getAt(i).raw.name,
           //     glyph:'9733@Entypo',

               cls:'bookmarkAssistantButton',
                textAlign:'left',
                margin:'10 10 0',
                width:300,
                padding:8,
                x:bX,
                y:20 + bY,
                style:{'font-size':'0.8em' , 'position':'absolute', 'font-weight':'normal !important'},
                menuActiveCls:'',
                clip:null,
                menu: [
                    {
                        text:'Show Storyboard',
                        bookmarkIndex:i,
                        activeCls:'',

                        handler:function(e){

                            me.showStory( bookmarks.getAt(this.bookmarkIndex).raw.snippets_id, bookmarks.getAt(this.bookmarkIndex).raw.snippets_filename );

                        }
                       },
                    {
                        text:'Copy Bookmark to Clipboard',
                        bookmarkIndex:i,
                        cls:'copyToClipboard',
                        handler:function(){
                            //this.up('homeview').addCls('bookmarAnimate');

                            this.up('button').animate({
                                duration: 1000,
                                from:{

                                   opacity:0.7
                                },
                                to: {


                                    opacity:1
                                }
                            });
                        }

                    },
                    {
                        text:'Print Storyboard',
                        activeCls:'',
                        bookmarkIndex:i,
                        handler:function(e){
                            window.open(ePatientChase.Utilities.clientURL + "index.html?print="+ bookmarks.getAt(this.bookmarkIndex).raw.url);
                      //      me.showPDFs( bookmarks.getAt(this.bookmarkIndex).raw.snippets_id, bookmarks.getAt(this.bookmarkIndex).raw.snippets_filename);
                        }

                    },
                    {
                        text:'Delete bookmark',
                        activeCls:'',
                        bookmarkIndex:i,
                        handler:function(e){
                            var myself = this;
                            Ext.Msg.show({
                                title:'Delete bookmark?',
                                msg: 'Do you want to delete this bookmark?',
                                buttons: Ext.Msg.YESNO,
                                test:"text",
                                fn:  function(text){

                                    if(text == 'yes'){


                                      var store =  Ext.getStore('Bookmarks');
                                        store.removeAt( myself.bookmarkIndex);
                                       store.sync();
                                        me.setContent();
                                        me.clearSnippets();
                                    }
                                }
                            });

                         //   me.showPDFs( bookmarks.getAt(this.bookmarkIndex).raw.snippets_id, bookmarks.getAt(this.bookmarkIndex).raw.snippets_filename);
                        }
                    }
                ],
                handler:function(){
                    var smID = ePatientChase.Utilities.clientURL +'index.html?bookmark=' + bookmarks.getAt(this.bookmarkIndex).raw.url;
                    this.menu.animate({
                       duration: 400,
                 //       easing:'easeIn',
                        from:{
                          opacity:0,
                            y: (this.getY()           -20  ),
                        //    width:0,
                            height:0
                        },
                        to: {
                            opacity: 1,
                            y: (this.getY()           +30),
                          //  width:200,
                            height:100
                        }
                    });

                    $('.copyToClipboard').zclip({
                        path:'resources/zclip/ZeroClipboard.swf',
                        copy:smID

                    });
                  /*
                    console.log("MY MENUS >>");


                    this.clip = new ZeroClipboard(jQuery('.copyToClipboard'));*/
                   // clip.setText( "Copy me!" );

                /*    var smID = ePatientChase.Utilities.clientURL +'index.html?bookmark=' + bookmarks.getAt(this.bookmarkIndex).raw.url;

                    ZeroClipboard.setDefaults( { moviePath: 'http://localhost:63342/epatientchase/Client/resources/Zeroclipboard-1.1.7/ZeroClipboard.swf' } );

                    */
                  /*  this.clip.setText('smIDw');
                    this.clip.on( 'click', function ( client, args ) {
                        console.log("zen I");
                        alert( "mouse button is down" );
                    } );

                    this.clip.addEventListener('mousedown',function() {
                        console.log("zen");
                     alert("chyj");
                    });

                    */
              }
            });

        }


        middleSection.doComponentLayout();



    },
    dataidCache:[],
    dataFileNameCache:[],
    showStory:function(dataid, dataFileName){

        var me = this;
        me.dataidCache = dataid;
        me.dataFileNameCache = dataFileName;
       this.clearSnippets();


       for(var s in dataid){
        setTimeout(function(){
            me.popSnippetToShow();

        }, (s+1)*30);

       }

    },
    popSnippetToShow:function(){
        var me = this;


        this.addSnippet(  me.dataidCache[0], me.dataFileNameCache[0]);
        me.dataidCache.splice(0,1);  me.dataFileNameCache.splice(0,1);
    },
    pdfArray:[],
    parseSnippetForPDF:function(snippetID, fileName){
        var me = this;
        var snippetURL = ePatientChase.Utilities.baseURL +'Content/'+ snippetID + '/' + fileName;

        Ext.Ajax.request({
            url:snippetURL,

            success: function(response){
                var data =JSON.parse( response.responseText);
                console.log(data);
                   for(var i in data.snippet.content){
                    if(data.snippet.content[i].link != undefined  && data.snippet.content[i].link != null && data.snippet.content[i].url != undefined)
                    {   console.log("###  " +data.snippet.content[i].url.indexOf('.pdf') );
                        if(data.snippet.content[i] == "section"){}
                        else if(data.snippet.content[i].url.indexOf('.pdf') != -1){
                            var theUrl  = me.getURL(data.snippet.content[i].url)

                            Ext.Ajax.request({
                                url:theUrl,

                                success: function(response){
                                    me.pdfArray.push(theUrl);
                                //     var win=window.open(theUrl, '_blank');
                                }, failure:function(response){  me.pdfArray.push(theUrl);
                                  //  var win=window.open(theUrl);
                            //        Ext.Msg.alert('Sorry!', 'File ' +theUrl+ '  not found.');
                                }
                            });


                        }
                    }
                   //        html.push('<a href="'+ +'" class="snippetLink" target="_blank">'+data.content[i].link +'</a> ');

                   }
            },
            failure:function(response){

            }
        });
    },
    showPDFs:function(dataid, dataFileName){
        var me = this;
        this.pdfArray = [];
        for(var s in dataid)
            this.parseSnippetForPDF(dataid[s], dataFileName[s]);
        for(var i = 0; i <5; i++){
          //  window.open('http://localhost:63342/epatientchase/Server/Resources/qofwithcombinedtargets.pdf', '_blank');
        }

        setTimeout(function(){
            Ext.Msg.show({
                title:'Show PDFs?',
                msg: 'Do you want to open several PDFs?',
                buttons: Ext.Msg.YESNO,
                test:"text",
                fn:  function(text){

                    if(text == 'yes'){
                        for(var i = 0; i < me.pdfArray.length; i++){
                        //    window.open(me.pdfArray[i], '_blank');
                     //       window.location.assign(me.pdfArray[i]);
                        }

                    }
                }
            });
        }, 1000);
    },
    getURL:function(theURL){
        var urlBase = theURL.replace('../../', ePatientChase.Utilities.baseURL);

        if(urlBase.indexOf("http") == -1 && urlBase.indexOf("https")){
            urlBase = ePatientChase.Utilities.baseURL + urlBase;
        }

        return urlBase;

    },
    showID:function(){
        var middleSection = this.getHomeview().child('middlesectionview');
        var labels =  middleSection.getComponent(1).query('label');
        for(var l in labels){
            labels[l].setVisible(true);
        }


        //.hidden = false;
    }
});