Ext.define('ePatientChase.view.Assistant',{
    extend:'Ext.Panel',
    xtype:'assistantview',
    id:'assistantview',

    items:[
        {
            xtype:'topsectionview'

        },
        {
            xtype:'middlesectionview',
            style:{'height':'auto'},
            layout:{

                type:'vbox',
                align:'center'
            },
            items:[
                {
                    xtype:'label',
                    id:'breadcrumbs',
                    text:'',
                    style:{'color':'#359db8', 'width':'auto' ,'font-size':'13px','margin-bottom':'0px','margin-top':'10px', 'margin-left':'0', 'left':'0'}
                },
                {
                    xtype:'container',

                    layout:{

                        type:'hbox',
                        pack:'center',
                        align:'stretch'
                    },
                    items:[

                {// previous button
                    xtype:'container',
                    layout:{type:'vbox'},
                    items:[ {
                        xtype:'button',
                        id:'prevStepBtn',
                        glyph:'59225@Entypo',
                        ui:'footer',
                        cls:'snippetPanelButton',
                        style:{'border-radius':'10px', 'margin-top':'100px','margin-bottom':'10px', 'margin-left':'0px','font-size':'140px !important' },
                        width:90,

                        hideMode:'offsets'
                    }
                      ,{
                        xtype:'label',
                        text:'Previous',
                        style:{'color':'#128cac'}
                        ,
                        margin:"-70 0 0 20"
                    }],
                    style:{'opacity':'0'}
                },


                {//carousel
                    xtype:'container',
                    layout:{type:'vbox'},
                    items:[

                        {
                            xtype:'label',
                            text:'Type of Recall',
                            id:'breadcrumbLabel',
                            style:{'color':'white', 'text-align':'center','width':'840px' , 'display':'box', 'margin-top':'10px','font-size':'22px',
                                'background-color': '#359db8','padding': '5px','border':'1px solid #359db8','border-radius': '5px'}
                            //   style:{'color':'#128cac', 'text-align':'center','width':'200px' , 'display':'box', 'margin-left':'635px', 'margin-top':'-20px','font-weight':'bolder'}

                        },

                        {
                            xtype:'carouselcontainer',
                            id:'carouselcontainer',
                            margin:'10 0 10 5'
                        }
                    ]
                }
                ,

                {//next button
                    xtype:'container',
                    layout:{type:'vbox'},
                    items:[{
                        xtype:'button',
                        id:'nextStepBtn',
                        glyph:'59226@Entypo',
                        //     html:'<p style="font-size:32px">Next Step</p>',
                        ui:'footer',
                        cls:'snippetPanelButton',
                        width:90,
                        style:{'border-radius':'10px', 'margin-top':'100px','margin-bottom':'10px','font-size':'140px !important', 'opacity':'1' }
                    },{
                        xtype:'label',
                        text:'Next',
                        style:{'color':'#128cac'},
                        margin:"-70 0 0 28"
                    },{
                        xtype:'button',
                        text:'bookmark',
                    //    padding:5,
                        glyph:'9733@Entypo',
                        hidden:true,
                        margin:'110 5 5 5',
                        cls:'bookmarkAssistantButton',
                        id:'bookmarkAssistantButton'
                    }
                    ]
                }


                    ]
                }

            ]
        },
        {
            xtype:'bottomsectionview'
        }
    ]

});
