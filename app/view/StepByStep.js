Ext.define('ePatientChase.view.StepByStep',{
    extend:'Ext.Panel',
    xtype:'stepbystepview',
    requires:['Ext.tree.Panel'],

    items:[
        {
            xtype:'topsectionview'

        },
        {
            xtype:'middlesectionview',
            layout:{

                type:'hbox',
                pack:'center',
                align:'center '
            },

            height:'100%',
            margin:10,
            items:[
                {

                    xtype:'container',
                    layout:{type:'vbox'},

                    items:[


                        {
                            xtype:'treepanel',

                            /*
                            dockedItems: [{
                                xtype: 'toolbar',
                                dock: 'top',
                                style:{'background-color':'#359db8'},
                                items: [
                                    {xtype:'label', text:'Choose you steps',top:1 , style:{'color':'#fff', 'font-size':'14px', 'top':'1 !important'}},
                                    { xtype: 'button', text: 'Reset', margin:'10 0 0 120', id:'stepbystepResetdButton', cls: 'x-btn-text-icon', icon: 'resources/CollapseFolderBlue.png' }
                                ]
                            }],*/

                            cls:'my-panel',

                            dockedItems: [
                                {   xtype:'container',
                                    style:{'background-color':'#359db8', 'height':'32px !important'},
                                    height:32,
                                    bodyPadding:0,

                                    items:[


                                {
                                    xtype: 'image',
                                    cls:'simpleButton',
                                    src: 'resources/CollapseFolderBlue.png',
                                    height: 28, // Specifying height/width ensures correct layout
                                    width: 28,
                                    renderTo:  Ext.getBody(),
                                    margin:'2 0 0 5',
                                    listeners: {
                                        render: function(c) {
                                            c.getEl().on('click', function(e) {

                                                this.up('stepbystepview').fireEvent("resetStoryboard");
                                            }, c);
                                        }
                                    }
                                }]
                                }
                                /*
                                {   xtype:'container',
                                    style:{'background-color':'#359db8'},
                                    height:32,
                                    items:[
                                        {
                                            xtype:'button',

                                            html: '<img src="resources/CollapseFolderBlue.png">',
                                            style:{'background':'none !important', 'border':'none'},
                                            margin:'2 0 0 0',
                                            padding:0,

                                            id:'stepbystepResetdButton',
                                            cls:'simpleButton'
                                        }
                                    ]
                                }*/
                            ],

                            id:"advancedTreePanel",

                            useArrows: true,
                            lines:false,

                            width: 330,
                            height:370,
                            margin:'0 0 0 0',
                            autoScroll:true,
                            rootVisible: true

                        }
                        ,  {
                            xtype:'button',
                            text:'Create Storyboard',
                            id:'stepbystepStoryBoardButton',
                            padding:5,
                            margin:'10 0 0 188'

                        }
                    ]
                }
                ,
                {
                    xtype:'carouselcontainer2',
                    id:'carouselcontainerStepbyStep',
                    margin:'0 0 0 30',width:640
                }

            ]
        },
        {
            xtype:'bottomsectionview'
        }
    ]

});