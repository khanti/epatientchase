Ext.define('ePatientChase.view.Viewport', {
    renderTo: Ext.getBody(),
    extend: 'Ext.container.Viewport',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border'
    ],
    id:'mainViewport',
    layout: {
        type: 'border'
    },

    items: [{
        region: 'north',
        xtype: 'container',
        height:25,
        frame:true,

        style:{'text-align':'right', 'background-color':'#359db8'},
        padding:'5 10 0 0',
        items:[
            {xtype:'label', text:'ePatientChase',  style:{ 'color':'#99BCE8'}},
            {
                xtype:'textfield',
                emptyText:'Search',
                id:'searchField',
                style:{'right':'10px',top:'30px', 'position':'absolute !important', 'border-radius':'5px !important' , 'z-index':'1000'}
            }]
    },{
        region: 'center',
        xtype: 'tabpanel',
        padding:0,
        plain: true,

    //    activeTab:4,
        items:[
            {
                title: 'Home',
                glyph: '8962@Entypo',
                xtype:'homeview',

                tabConfig:{
                width:100,
                margin:0
            }
            },
            {
                title:'Basic',
                glyph:'59160@Entypo',
                xtype:'assistantview',
                tabConfig:{
                 //   width:120

                }


            },
            {
                title:'Advanced',
                glyph: '128203@Entypo',
                xtype:'stepbystepview',
                tabConfig:{
                    width:140,

                    margin:0,
                    style:{ 'margin-left':'10px', 'margin-right':'10px', 'text-align':'left'}
                }
            },
            //{
            //    title:'Q&A',
            //    glyph: '59140@Entypo',
            //    xtype:'qaview'
            //
            //
            //},
            {
                title:'Reference',
                glyph: '128213@Entypo',
                xtype:'referenceview'
            },
            {
                title:'Search',
                id:'searchTab',
                glyph: '128269@Entypo',
                xtype:'searchview'
            }
        ]
    }
    ]


});