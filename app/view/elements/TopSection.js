Ext.define('ePatientChase.view.elements.TopSection', {
    extend: 'Ext.Container',
    xtype: 'topsectionview',
    height:70,
   // width:'100%',
    padding:'0 10 0 5',
    layout:{
        type:'vbox',

        align: 'center',
        pack:'center'
    },


    tpl:['<h1 style="margin-bottom: 0;">{Title}</h1><span style="font-size:13px; font-weight: normal">{Description}</span><p>{Paragraph}</p>'
    ],
    style:{'border':'1px #99bce8 solid' }
});