Ext.define('ePatientChase.view.elements.CarouselContainer2', {
    extend: 'Ext.Container',
    xtype: 'carouselcontainer2',
    //   id:'thecarousel',
    width:840,
    height:360,
    style:{'border':'none'},

    setCarouselData:function(data, firstSlide){

        var i;
        var myHTML =
            '<div style="">' +
                '<div id="amazingslider-2" style="display:block" >' +
                '  <ul class="amazingslider-slides" style="display:none;">';
        var imgURL;
        for(i = 0 ; i < data.length; i++){
            console.log(data[i].name + " " +  data[i].summary);
            imgURL = this.getURL(data[i].image);
            myHTML += '<li ><img onclick="alert()" src="'+imgURL+'" alt="'+ data[i].name+ '" data-description="'+ data[i].summary+'" /></li>';
        }

        //  style="width:auto; height: 340px !important;"



        myHTML +=    '</ul>'  +
                '<div class="amazingslider-engine" style="display:none;"><a href="http://amazingslider.com">jQuery Image Slider</a></div>' +
                '</div>' +
                '</div>';




        this.update({'html':myHTML})

        this.initCarousel(firstSlide);
    },
    getURL:function(theURL){
        if(theURL == undefined) theURL = "/Resources/logo_large.jpg" ;

        var urlBase = theURL.replace('../../', ePatientChase.Utilities.baseURL);

        if(urlBase.indexOf("http") == -1 && urlBase.indexOf("https")){
            urlBase = ePatientChase.Utilities.baseURL + urlBase;
        }

        return urlBase;

    },
    initCarousel:function(firstSlide){
        //var jsFolder = "resources/carousel/sliderengine/skins/";
        var me = this;
        var jsFolder = "resources/carousel/sliderengine/";
        var car = jQuery("#amazingslider-2").amazingslider({
            jsfolder:jsFolder,
            width:600,
            height:340,
            watermarkstyle:"text",
            loadimageondemand:false,
            watermarktext:"amazingslider.com",
            isresponsive:true,
            autoplayvideo:false,
            watermarkimage:"",
            pauseonmouseover:false,
            watermarktextcss:"font:12px Arial,Tahoma,Helvetica,sans-serif;color:#333;padding:2px 4px;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;background-color:#fff;opacity:0.9;filter:alpha(opacity=90);",
            addmargin:true,
            randomplay:false,
            showwatermark:false,
            watermarklinkcss:"text-decoration:none;font:12px Arial,Tahoma,Helvetica,sans-serif;color:#333;",
            slideinterval:5000,
            watermarktarget:"_blank",
            watermarkpositioncss:"display:block;position:absolute;bottom:4px;right:4px;",
            watermarklink:"http://amazingslider.com?source=watermark",
            enabletouchswipe:true,
            transitiononfirstslide:false,
            loop:0,
            autoplay:false,
            navplayvideoimage:"play-32-32-0.png",
            navpreviewheight:60,
            timerheight:2,
            shownumbering:false,
            skin:"FeatureList",
            addgooglefonts:true,
            navshowplaypause:true,
            navshowplayvideo:true,
            navbuttonradius:0,
            navpreviewposition:"top",
            navmarginy:16,
            showshadow:false,
            navfeaturedarrowimagewidth:8,
            navpreviewwidth:0,
            googlefonts:"Noto Sans",
            textpositionmarginright:24,
            bordercolor:"#50c8e5",
            navthumbnavigationarrowimagewidth:32,
            navthumbtitlehovercss:"",
            navcolor:"#999999",
            arrowwidth:32,
            texteffecteasing:"easeOutCubic",
            texteffect:"slide",
            navspacing:4,
            playvideoimage:"playvideo-64-64-0.png",
            ribbonimage:"ribbon_topleft-0.png",
            navwidth:70,
            navheight:70,
            arrowimage:"arrows-32-32-1.png",
            timeropacity:0.6,
            navthumbnavigationarrowimage:"carouselarrows-32-32-1.png",
            navpreviewbordercolor:"#ffffff",
            ribbonposition:"topleft",
            navthumbdescriptioncss:"display:block;position:relative;padding:2px 4px;text-align:left;font:normal 11px Arial,Helvetica,sans-serif;color:#eee;",
            arrowstyle:"mouseover",
            navthumbtitleheight:18,
            textpositionmargintop:24,
            navswitchonmouseover:false,
            navarrowimage:"navarrows-28-28-0.png",
            arrowtop:50,
            textstyle:"static",
            playvideoimageheight:64,
            navfonthighlightcolor:"#666666",
            showbackgroundimage:false,
            navpreviewborder:4,
            shadowcolor:"#aaaaaa",
            navbuttonshowbgimage:false,
            navbuttonbgimage:"navbuttonbgimage-28-28-0.png",
            navthumbnavigationarrowimageheight:32,
            textbgcss:"display:block; position:absolute; top:0px; left:0px; width:100%; height:100%; background-color:#333333; opacity:0.6; filter:alpha(opacity=60);",
            playvideoimagewidth:64,
            navborder:2,
            bottomshadowimagewidth:110,
            showtimer:false,
            navradius:0,
            navshowpreview:false,
            navpreviewarrowheight:8,
            navmarginx:16,
            navfeaturedarrowimage:"featuredarrow-8-16-0.png",
            showribbon:false,
            navfeaturedarrowimageheight:16,
            navstyle:"none",
            textpositionmarginleft:24,
            descriptioncss:"display:block; position:relative; margin-top:4px; font:12px Inder,Arial,Tahoma,Helvetica,sans-serif; color:#fff;",
            navplaypauseimage:"navplaypause-28-28-0.png",
            backgroundimagetop:-10,
            timercolor:"#ffffff",
            numberingformat:"%NUM/%TOTAL ",
            navfontsize:12,
            navhighlightcolor:"#333333",
            navimage:"bullet-24-24-5.png",
            navbuttoncolor:"#999999",
            navshowarrow:true,
            navshowfeaturedarrow:true,
            titlecss:"display:block; position:relative; font: 14px Inder,Arial,Tahoma,Helvetica,sans-serif; color:#fff;",
            ribbonimagey:0,
            ribbonimagex:0,
            shadowsize:5,
            arrowhideonmouseleave:1000,
            navopacity:0.5,
            backgroundimagewidth:120,
            textautohide:false,
            navthumbtitlewidth:150,
            arrowheight:32,
            arrowmargin:8,
            texteffectduration:1000,
            bottomshadowimage:"bottomshadow-110-95-4.png",
            border:4,
            timerposition:"bottom",
            navfontcolor:"#333333",
            navthumbnavigationstyle:"auto",
            borderradius:5,
            navbuttonhighlightcolor:"#333333",
            textpositionstatic:"bottom",
            navthumbstyle:"imageandtitledescription",
            textcss:"display:block; padding:12px; text-align:left;",
            navbordercolor:"#359db8",
            navpreviewarrowimage:"previewarrow-16-8-0.png",
            showbottomshadow:true,
            navdirection:"vertical",
            textpositionmarginstatic:0,
            backgroundimage:"",
            navposition:"right",
            navpreviewarrowwidth:16,
            bottomshadowimagetop:95,
            textpositiondynamic:"bottomleft",
            navshowbuttons:false,
            navthumbtitlecss:"display:block;position:relative;padding:2px 4px;text-align:left;font:bold 12px Arial,Helvetica,sans-serif;color:#fff;",
            textpositionmarginbottom:24,
            ribbonmarginy:0,
            ribbonmarginx:0,
            fade: {
                duration:1,
                easing:"easeOutCubic",
                checked:true
            },
            transition:"fade",
            firstElement:1,

            itemchanged:function(a){
          //      me.up('stepbystepview').fireEvent("carouselItemChanged",{itemIndx:a});

                //me.fireEvent("carouselItemChanged",me);
            },
            itemclicked:function(a){
                me.up('stepbystepview').fireEvent("carouselItemClicked",{itemIndx:a});
            }
        })  ;
        /// jQuery("#amazingslider-1").amazingslider().getParams();
        //   jQuery("#amazingslider-1").html5lightbox.setSlide(2);
        //  car[0].options.setSlide(1);
        console.log(GLOB);
           GLOB.setSlide(firstSlide);

    }

});