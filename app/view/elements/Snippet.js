Ext.define('ePatientChase.view.elements.Snippet', {
    extend: 'Ext.button.Button',
    xtype: 'snippetview',

    width:'250px',
    height:'200px',

    padding:10,
    html:'snippet',
    cls:'snippet',

//    tpl:['<h3>snippet</h3>'    ],
    style:{'border':'1px #99bce8 solid', 'text-align':'center', 'margin':'10px', 'display':'inline-block', 'float':'left', 'width':'250px'},
    snippetData:null,
    snippetURL:'',
    previousOpacity:1,
    fadeTo:function(val){
        this.previousOpacity = val;
        this.stopAnimation();
        this.animate({
            easing:'ease',
            duration:100,
            to: {
                opacity:val

            }
        });
    },
    fadeBack:function(){
        this.animate({
            easing:'ease',
            duration:100,
            to: {
                opacity:this.previousOpacity

            }
        });
    },
    listners:{

    }
});