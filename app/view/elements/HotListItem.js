Ext.define('ePatientChase.view.elements.HotListItem', {
    extend: 'Ext.button.Button',
    xtype: 'hotlistitem',
    width:200,
    height:40,
    margin:'0 0 5 0',
    toggleGroup:'HotList',
    enableToggle: true,
    baseCls:'HotListItem',

    pressedCls:'HotListItemPressed'
});