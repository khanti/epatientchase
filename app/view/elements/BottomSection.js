Ext.define('ePatientChase.view.elements.BottomSection', {
    extend: 'Ext.Panel',
    xtype: 'bottomsectionview',

  //  html:'BottomSection',


      height:'25%',
    width:'100%',


    layout:{
        type: 'hbox',
        pack: 'start',
        align: 'stretch'
    },

    style:{ 'position':'absolute', 'bottom':'0', 'min-height':'120px','border':'1px #99bce8 solid !important' },


    items:[
        {
            xtype:'button',

            glyph:'59225@Entypo',
            ui:'footer',
            cls:'snippetPanelButton',
            style:{'border-radius':'10px', 'margin-top':'10px','margin-bottom':'10px','font-size':'140px !important', 'opacity':'0' },
            width:90,

            hideMode:'offsets',
         //   hidden:true,
            handler:function(btn){

                var me = this.up('bottomsectionview');
                var scrollVal = 1 * 270;

                if(  me.currentScroll <= 0 ){
                    //hide right button
                }
                else{
                    me.child('container').scrollBy(-scrollVal,0,true);
                    me.currentScroll--;

                    me.getComponent(2).animate({to:{'opacity':'1'}});
                    if(  me.currentScroll <= 0 ){
                        me.getComponent(0).animate({to:{'opacity':'0'}});
                    }
                }

            }
        },{
            xtype:'container',
            width:1000,
            height:220,
            style:{'overflow':'hidden'},
            items:[{
                xtype:'snippetcontainer',
                style:{'border':'none' }

            }]


        },
        {
            xtype:'button',

            glyph:'59226@Entypo',
            ui:'footer',
            cls:'snippetPanelButton',
            width:90,
            style:{'border-radius':'10px', 'margin-top':'10px','margin-bottom':'10px','font-size':'140px !important', 'opacity':'0' },
    //        hidden:true,
            handler:function(btn){
                var me = this.up('bottomsectionview');
                var scrollVal = 1 * 270;

                if(  me.currentScroll >= (me.snippetCount -  me.maxSnips)){
                    //hide right button
                }
                else{
                    me.child('container').scrollBy(scrollVal,0,true);
                    me.currentScroll++;

                    me.getComponent(0).animate({to:{'opacity':'1'}});
                    if( me.currentScroll >= ( me.snippetCount -  me.maxSnips)){
                        me.getComponent(2).animate({to:{'opacity':'0'}});
                    }
                }

        }
        }
    ],

    listeners:{
      resize:function (me, width, height, oldWidth, oldHeight, eOpts ){
          var snipW = 270;
          var initW = this.getWidth();
          var newW;

          this.initialWidth = initW - 180;
          this.maxSnips = parseInt( this.initialWidth/snipW);
          this.getComponent(1).setWidth(this.maxSnips*snipW);

          newW = 180 + this.maxSnips*snipW;

          if(window.innerHeight < 800){

          }

          var marginValue = (initW - newW)/2;
          me.setBodyStyle('padding-left',marginValue+"px");
     //     alert("max snips: " + this.maxSnips);
    //      console.log(this.id + "  "+me.id);
      }
    },
    snippetCount:0,
    initialWidth:0,
    maxSnips:0,
    currentScroll:0,
    addSnippet:function(data, snippetURL){
        var me = this;
        this.child('container').child('snippetcontainer').addSnippet(data, snippetURL);



        this.snippetCount++;
        console.log(this.snippetCount* 250 + " " + this.child('container').child('snippetcontainer').getWidth());
        if( this.snippetCount  >this.maxSnips){



      //   this.getComponent(0).animate({to:{'opacity':'1'}});
         this.getComponent(2).animate({to:{'opacity':'1'}});
        //    this.child('container').child('snippetcontainer').

       ///  me.child('snippetcontainer').setWidth(600);
        }
    },
    clearSnippets:function(){
        this.snippetCount = 0;
     //   this.child('container').scrollBy(scrollVal,0,true);
        this.currentScroll = 0;
        this.getComponent(0).animate({to:{'opacity':'0'}});
        this.child('container').child('snippetcontainer').removeAll();
        this.getComponent(2).animate({to:{'opacity':'0'}});
    }




});