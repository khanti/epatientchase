Ext.define("ePatientChase.view.SnippetContainer", {
    extend: 'Ext.Panel',
    xtype:'snippetcontainer',
    style:{'border':'0px' ,  'padding':'0','margin': '0' },
    width:3000,
    height:'30%',
    layout:'anchor',
    border:false,
   // overflowX:'auto',
    addSnippet:function(data, snippetURL){
        var imgURL;
        var bVideo = false;

        var thumbHTML ='<div class="snippetTitle">'+data.snippet.title+'</div>'+
            '<div class="snippetSummary">'+data.snippet.summary+'</div>';

        if(data.snippet.video != undefined){

            var videoID =data.snippet.video.replace('http://www.youtube.com/watch?v=','');
            imageURL = 'http://img.youtube.com/vi/'+videoID+'/0.jpg';
            bVideo = true;
        }

            for(var i = 0 ; i < data.snippet.content.length; i++){

                if(data.snippet.content[i].image != null){
                    imageURL = this.getURL(data.snippet.content[i].image);
                    break;
                }
            }


        var rand = Math.random()
        /*
        if( rand < 0.33){
            bVideo =false;
            for(var i = 0 ; i < data.snippet.content.length; i++){

                if(data.snippet.content[i].image != null){
                    imageURL = this.getURL(data.snippet.content[i].image);
                    break;
                }
            }
        }
        else if( rand < 0.6){
            bVideo =false;
            imageURL = this.getURL('../../Resources/logo.jpg');
        }*/


        thumbHTML+='<div class="snippetImage"><img src="'+imageURL+'" />'

        if(bVideo == true)
        {
          thumbHTML +=  '<div class="snippetImageOverlay">&#9654;</div>';
        }
            thumbHTML+= '</div>';

        thumbHTML +=    '<div class="snippetOveraly">&#128269;</div>';


        this.add({'xtype':'snippetview',html:thumbHTML, snippetData:data, snippetURL:snippetURL});


    },
    getURL:function(theURL){
        var urlBase = theURL.replace('../../', ePatientChase.Utilities.baseURL);

        if(urlBase.indexOf("http") == -1 && urlBase.indexOf("https")){
            urlBase = ePatientChase.Utilities.baseURL + urlBase;
        }

        return urlBase;

    }
});