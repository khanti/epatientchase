Ext.define('ePatientChase.view.Home',{
    extend:'Ext.Panel',
    xtype:'homeview',

    items:[
        {
            xtype:'topsectionview'

        },
        {
            xtype:'middlesectionview',

            padding:10,
            frame:false,

            items:[
                {xtype:'container', tpl:[]/*,
                    items:[
                        {
                            xtype:'button',
                            text:'Manage Bookmarks',
                            glyph:'9881@Entypo',
                            cls:'bookmarkAssistantButton',
                            style:{'float':'right'},
                            menu: [{
                                text:'Share',
                                id:'showIDButton'
                            }
                            ]
                        }]*/
                },
                {xtype:'container'  ,overflow:'auto',layout:{type:'column'}}
            ]
        },
        {
            xtype:'bottomsectionview'

        }
    ]

});