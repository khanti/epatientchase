Ext.define('ePatientChase.view.Reference',{
    extend:'Ext.Panel',
    xtype:'referenceview',

    items:[
        {
            xtype:'topsectionview'

        },
        {
            xtype:'middlesectionview',
            layout:{
                type:'hbox',
                align:'center',
                pack:'center'
            },
            items:[
                {
                xtype:'treepanel',
                    cls:'my-panel',
                    /*      tools: [

                    { type:'minus' , id:'refResetButton'  }

                ],*/
             //   cls:'tree_wrap',

                dockedItems: [
                    {   xtype:'container',
                        style:{'background-color':'#359db8', 'height':'32px !important'},
                        height:32,
                        bodyPadding:0,

                        items:[

                            {
                                xtype: 'image',
                                cls:'simpleButton',
                                src: 'resources/CollapseFolderBlue.png',
                                height: 28, // Specifying height/width ensures correct layout
                                width: 28,
                                renderTo:  Ext.getBody(),
                                margin:'2 0 0 5',

                                listeners: {
                                    render: function(c) {
                                        c.getEl().on('click', function(e) {

                                            this.up('referenceview').fireEvent("resetRef");
                                        }, c);
                                    }
                                }
                            }]}

                    /*
                    {

                    xtype: 'toolbar',
                    dock: 'top',
                    style:{'background-color':'#359db8'},

                    height:32,

                    items: [
                        // {xtype:'label', text:'Choose you steps',top:1 , style:{'color':'#fff', 'font-size':'14px', 'top':'1 !important'}},
                        { xtype: 'button', text: 'Collapse All', margin:'10 0 0 0', id:'refResetButton', cls: 'x-btn-icon', icon: 'resources/CollapseFolderBlue.png',
                            style:{'background-coloe':'none !important'}}
                    ]
                }*/

                ],
                id:"refTreePanel",

                useArrows: true,
                lines:false,

                width: 400,
                height:370,
                margin:'10 0 0 0',
                autoScroll:true,
                rootVisible: false

            },
                {
                    xtype:'carouselcontainer3',
                    id:'carouselcontainerReference',
                    margin:'10 0 0 30',width:640
                }
            ]
        },
        {
            xtype:'bottomsectionview'
        }
    ]

});