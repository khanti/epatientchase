Ext.define('ePatientChase.view.QA',{
    extend:'Ext.Panel',
    xtype:'qaview',

    items:[
        {
            xtype:'topsectionview'

        },
        {
            xtype:'middlesectionview',
            layout:{
                type:'hbox',
                align:'center',
                pack:'center'
            },
            items:[{
                xtype:'treepanel',
                cls:'tree_wrap',


                dockedItems: [

                    {   xtype:'container',
                        style:{'background-color':'#359db8', 'height':'32px !important'},
                        height:32,
                       bodyPadding:0,
                        id:'dock1',
                        items:[

                            {
                                xtype: 'image',
                                cls:'simpleButton',
                                src: 'resources/CollapseFolderBlue.png',
                                height: 28, // Specifying height/width ensures correct layout
                                width: 28,
                                renderTo:  Ext.getBody(),
                                margin:'2 0 0 5',

                                listeners: {
                                    render: function(c) {
                                        c.getEl().on('click', function(e) {

                                           this.up('qaview').fireEvent("resetQA");
                                        }, c);
                                    }
                                }
                            }
                                /* {
                                xtype:'button',
                                height:32,
                                html: '<img src="resources/CollapseFolderBlue.png">',
                                style:{'background':'none !important', 'border':'none'},
                                margin:'2 0 0 0',

                                padding:0,
                                id:'qaResetButton',
                                cls:'simpleButton'

                                //,                                renderTo:  Ext.getBody()

                            } */
                        ]
                    }],
                /*
                dockedItems: [{
                    xtype: 'toolbar',
                    dock: 'top',
                    style:{'background-color':'#359db8'},
                    items: [
                       // {xtype:'label', text:'Choose you steps',top:1 , style:{'color':'#fff', 'font-size':'14px', 'top':'1 !important'}},
                        { xtype: 'button', text: 'Collapse All', margin:'10 0 0 0', id:'qaResetButton', cls: 'x-btn-text-icon', icon: 'resources/CollapseFolderBlue.png' },
                    ]
                }],*/
                id:"qaTreePanel",

                useArrows: true,
                lines:false,

                width: 800,
                height:370,
                margin:'10 0 0 0',
                autoScroll:true,
                rootVisible: false

            }]
        },
        {
            xtype:'bottomsectionview'
        }
    ]

});