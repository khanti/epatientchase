Ext.define('ePatientChase.view.Search',{
    extend:'Ext.Panel',
    xtype:'searchview',

    items:[
        {
            xtype:'topsectionview'

        },
        {
            xtype:'middlesectionview',
            layout:{
                type:'hbox',
                align:'center',
                pack:'center'
            },
            items:[{
                xtype:'container',
                layout:{type:'vbox'},

             items:[{
                 xtype:'treepanel',
                 cls:'my-panel',
                 /*      tools: [

                  { type:'minus' , id:'refResetButton'  }

                  ],*/
                 //   cls:'tree_wrap',

                 dockedItems: [
                     {   xtype:'container',
                         style:{'background-color':'#359db8', 'height':'32px !important'},
                         height:32,
                         bodyPadding:0,

                         items:[

                             {
                                 xtype: 'image',
                                 cls:'simpleButton',
                                 src: 'resources/CollapseFolderBlue.png',
                                 height: 28, // Specifying height/width ensures correct layout
                                 width: 28,
                                 renderTo:  Ext.getBody(),
                                 margin:'2 0 0 5',

                                 listeners: {
                                     render: function(c) {
                                         c.getEl().on('click', function(e) {

                                             this.up('referenceview').fireEvent("resetRef");
                                         }, c);
                                     }
                                 }
                             }]}



                 ],

                 id:"refTreePanel2",

                 useArrows: true,
                 lines:false,

                 width: 400,
                 height:370,
                 margin:'10 0 0 0',
                 autoScroll:true,
                 rootVisible: false

             },{
                 xtype:'button',
                 text:'Create Storyboard',
                 id:'searchStoryBoardButton',
                 padding:5,
                 margin:'10 0 0 270'
             }]
            }

                ,
                {
                    xtype:'carouselcontainer4',
                    id:'carouselcontainerSearch',
                    margin:'10 0 0 30',width:640
                }
            ]
        },
        {
            xtype:'bottomsectionview'
        }
    ]

});