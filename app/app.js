/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

// DO NOT DELETE - this directive is required for Sencha Cmd packages to work.
//@require @packageOverrides


var myApp = Ext.application({

    name: 'ePatientChase',
    requires:[
        'ePatientChase.Utilities',
        'Ext.layout.container.Column',
        'Ext.data.proxy.LocalStorage',
        'Ext.DomHelper',
        'Ext.LoadMask'
    ],
    views: [
        'Assistant',
        'Home',
        'Main',
        'QA',
        'Reference',
        'SnippetContainer',
        'StepByStep',
        'Viewport',
        'elements.BottomSection',
        'elements.MiddleSection',
        'elements.Snippet',
        'elements.TopSection',
        'elements.CarouselContainer',
        'elements.CarouselContainer2',
        'elements.CarouselContainer3',
        'elements.CarouselContainer4',
        'elements.HotListContainer',
        'elements.HotListItem',
        'PrintView',
        'Search'
    ],


    stores: [
        'Assistant',
        'Home',
        'Flow',
        'QA',
        'Reference',
        'StepByStep',
        'Bookmarks',
        'Search'
    ],


    controllers: [
        'TabController',
        'Assistant',
        'Home',
        'Main',
        'QA',
        'Reference',
        'Snippet',
        'StepByStep',
        'PrintView',
        'Search'
    ],

    autoCreateViewport: true,
    init:function(){

        StackMob.init({
            publicKey: "56a7e668-3f15-49ad-9153-7e0e4b3e04ec",
            apiVersion: 0
        });

    },
    launch:function( application ){
        if (Ext.get('page-loader')) {
            Ext.get('page-loader').remove();
        }

    }
});

